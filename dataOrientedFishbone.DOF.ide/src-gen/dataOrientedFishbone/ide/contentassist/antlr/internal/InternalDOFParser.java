package dataOrientedFishbone.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import dataOrientedFishbone.services.DOFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDOFParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'dof'", "'import'", "'effect'", "'is'", "'include'", "'category'", "'otras'", "'cause'", "'contains'", "'{'", "'}'", "'realizes'", "'notMapped'", "'.*'", "'.'"
    };
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=5;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDOFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDOFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDOFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDOF.g"; }


    	private DOFGrammarAccess grammarAccess;

    	public void setGrammarAccess(DOFGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleDOF"
    // InternalDOF.g:53:1: entryRuleDOF : ruleDOF EOF ;
    public final void entryRuleDOF() throws RecognitionException {
        try {
            // InternalDOF.g:54:1: ( ruleDOF EOF )
            // InternalDOF.g:55:1: ruleDOF EOF
            {
             before(grammarAccess.getDOFRule()); 
            pushFollow(FOLLOW_1);
            ruleDOF();

            state._fsp--;

             after(grammarAccess.getDOFRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDOF"


    // $ANTLR start "ruleDOF"
    // InternalDOF.g:62:1: ruleDOF : ( ( rule__DOF__Group__0 ) ) ;
    public final void ruleDOF() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:66:2: ( ( ( rule__DOF__Group__0 ) ) )
            // InternalDOF.g:67:2: ( ( rule__DOF__Group__0 ) )
            {
            // InternalDOF.g:67:2: ( ( rule__DOF__Group__0 ) )
            // InternalDOF.g:68:3: ( rule__DOF__Group__0 )
            {
             before(grammarAccess.getDOFAccess().getGroup()); 
            // InternalDOF.g:69:3: ( rule__DOF__Group__0 )
            // InternalDOF.g:69:4: rule__DOF__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DOF__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDOF"


    // $ANTLR start "entryRuleImport"
    // InternalDOF.g:78:1: entryRuleImport : ruleImport EOF ;
    public final void entryRuleImport() throws RecognitionException {
        try {
            // InternalDOF.g:79:1: ( ruleImport EOF )
            // InternalDOF.g:80:1: ruleImport EOF
            {
             before(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getImportRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalDOF.g:87:1: ruleImport : ( ( rule__Import__Group__0 ) ) ;
    public final void ruleImport() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:91:2: ( ( ( rule__Import__Group__0 ) ) )
            // InternalDOF.g:92:2: ( ( rule__Import__Group__0 ) )
            {
            // InternalDOF.g:92:2: ( ( rule__Import__Group__0 ) )
            // InternalDOF.g:93:3: ( rule__Import__Group__0 )
            {
             before(grammarAccess.getImportAccess().getGroup()); 
            // InternalDOF.g:94:3: ( rule__Import__Group__0 )
            // InternalDOF.g:94:4: rule__Import__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleEffect"
    // InternalDOF.g:103:1: entryRuleEffect : ruleEffect EOF ;
    public final void entryRuleEffect() throws RecognitionException {
        try {
            // InternalDOF.g:104:1: ( ruleEffect EOF )
            // InternalDOF.g:105:1: ruleEffect EOF
            {
             before(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getEffectRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalDOF.g:112:1: ruleEffect : ( ( rule__Effect__Group__0 ) ) ;
    public final void ruleEffect() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:116:2: ( ( ( rule__Effect__Group__0 ) ) )
            // InternalDOF.g:117:2: ( ( rule__Effect__Group__0 ) )
            {
            // InternalDOF.g:117:2: ( ( rule__Effect__Group__0 ) )
            // InternalDOF.g:118:3: ( rule__Effect__Group__0 )
            {
             before(grammarAccess.getEffectAccess().getGroup()); 
            // InternalDOF.g:119:3: ( rule__Effect__Group__0 )
            // InternalDOF.g:119:4: rule__Effect__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalDOF.g:128:1: entryRuleCategory : ruleCategory EOF ;
    public final void entryRuleCategory() throws RecognitionException {
        try {
            // InternalDOF.g:129:1: ( ruleCategory EOF )
            // InternalDOF.g:130:1: ruleCategory EOF
            {
             before(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getCategoryRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalDOF.g:137:1: ruleCategory : ( ( rule__Category__Group__0 ) ) ;
    public final void ruleCategory() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:141:2: ( ( ( rule__Category__Group__0 ) ) )
            // InternalDOF.g:142:2: ( ( rule__Category__Group__0 ) )
            {
            // InternalDOF.g:142:2: ( ( rule__Category__Group__0 ) )
            // InternalDOF.g:143:3: ( rule__Category__Group__0 )
            {
             before(grammarAccess.getCategoryAccess().getGroup()); 
            // InternalDOF.g:144:3: ( rule__Category__Group__0 )
            // InternalDOF.g:144:4: rule__Category__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleDataFeeder"
    // InternalDOF.g:153:1: entryRuleDataFeeder : ruleDataFeeder EOF ;
    public final void entryRuleDataFeeder() throws RecognitionException {
        try {
            // InternalDOF.g:154:1: ( ruleDataFeeder EOF )
            // InternalDOF.g:155:1: ruleDataFeeder EOF
            {
             before(grammarAccess.getDataFeederRule()); 
            pushFollow(FOLLOW_1);
            ruleDataFeeder();

            state._fsp--;

             after(grammarAccess.getDataFeederRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataFeeder"


    // $ANTLR start "ruleDataFeeder"
    // InternalDOF.g:162:1: ruleDataFeeder : ( ( rule__DataFeeder__Group__0 ) ) ;
    public final void ruleDataFeeder() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:166:2: ( ( ( rule__DataFeeder__Group__0 ) ) )
            // InternalDOF.g:167:2: ( ( rule__DataFeeder__Group__0 ) )
            {
            // InternalDOF.g:167:2: ( ( rule__DataFeeder__Group__0 ) )
            // InternalDOF.g:168:3: ( rule__DataFeeder__Group__0 )
            {
             before(grammarAccess.getDataFeederAccess().getGroup()); 
            // InternalDOF.g:169:3: ( rule__DataFeeder__Group__0 )
            // InternalDOF.g:169:4: rule__DataFeeder__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataFeederAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataFeeder"


    // $ANTLR start "entryRuleCause"
    // InternalDOF.g:178:1: entryRuleCause : ruleCause EOF ;
    public final void entryRuleCause() throws RecognitionException {
        try {
            // InternalDOF.g:179:1: ( ruleCause EOF )
            // InternalDOF.g:180:1: ruleCause EOF
            {
             before(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalDOF.g:187:1: ruleCause : ( ( rule__Cause__Alternatives ) ) ;
    public final void ruleCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:191:2: ( ( ( rule__Cause__Alternatives ) ) )
            // InternalDOF.g:192:2: ( ( rule__Cause__Alternatives ) )
            {
            // InternalDOF.g:192:2: ( ( rule__Cause__Alternatives ) )
            // InternalDOF.g:193:3: ( rule__Cause__Alternatives )
            {
             before(grammarAccess.getCauseAccess().getAlternatives()); 
            // InternalDOF.g:194:3: ( rule__Cause__Alternatives )
            // InternalDOF.g:194:4: rule__Cause__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Cause__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getCauseAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleCompoundCause"
    // InternalDOF.g:203:1: entryRuleCompoundCause : ruleCompoundCause EOF ;
    public final void entryRuleCompoundCause() throws RecognitionException {
        try {
            // InternalDOF.g:204:1: ( ruleCompoundCause EOF )
            // InternalDOF.g:205:1: ruleCompoundCause EOF
            {
             before(grammarAccess.getCompoundCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleCompoundCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCompoundCause"


    // $ANTLR start "ruleCompoundCause"
    // InternalDOF.g:212:1: ruleCompoundCause : ( ( rule__CompoundCause__Group__0 ) ) ;
    public final void ruleCompoundCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:216:2: ( ( ( rule__CompoundCause__Group__0 ) ) )
            // InternalDOF.g:217:2: ( ( rule__CompoundCause__Group__0 ) )
            {
            // InternalDOF.g:217:2: ( ( rule__CompoundCause__Group__0 ) )
            // InternalDOF.g:218:3: ( rule__CompoundCause__Group__0 )
            {
             before(grammarAccess.getCompoundCauseAccess().getGroup()); 
            // InternalDOF.g:219:3: ( rule__CompoundCause__Group__0 )
            // InternalDOF.g:219:4: rule__CompoundCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCompoundCause"


    // $ANTLR start "entryRuleDataLinkedCause"
    // InternalDOF.g:228:1: entryRuleDataLinkedCause : ruleDataLinkedCause EOF ;
    public final void entryRuleDataLinkedCause() throws RecognitionException {
        try {
            // InternalDOF.g:229:1: ( ruleDataLinkedCause EOF )
            // InternalDOF.g:230:1: ruleDataLinkedCause EOF
            {
             before(grammarAccess.getDataLinkedCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleDataLinkedCause();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDataLinkedCause"


    // $ANTLR start "ruleDataLinkedCause"
    // InternalDOF.g:237:1: ruleDataLinkedCause : ( ( rule__DataLinkedCause__Group__0 ) ) ;
    public final void ruleDataLinkedCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:241:2: ( ( ( rule__DataLinkedCause__Group__0 ) ) )
            // InternalDOF.g:242:2: ( ( rule__DataLinkedCause__Group__0 ) )
            {
            // InternalDOF.g:242:2: ( ( rule__DataLinkedCause__Group__0 ) )
            // InternalDOF.g:243:3: ( rule__DataLinkedCause__Group__0 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getGroup()); 
            // InternalDOF.g:244:3: ( rule__DataLinkedCause__Group__0 )
            // InternalDOF.g:244:4: rule__DataLinkedCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDataLinkedCause"


    // $ANTLR start "entryRuleNotMappedCause"
    // InternalDOF.g:253:1: entryRuleNotMappedCause : ruleNotMappedCause EOF ;
    public final void entryRuleNotMappedCause() throws RecognitionException {
        try {
            // InternalDOF.g:254:1: ( ruleNotMappedCause EOF )
            // InternalDOF.g:255:1: ruleNotMappedCause EOF
            {
             before(grammarAccess.getNotMappedCauseRule()); 
            pushFollow(FOLLOW_1);
            ruleNotMappedCause();

            state._fsp--;

             after(grammarAccess.getNotMappedCauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleNotMappedCause"


    // $ANTLR start "ruleNotMappedCause"
    // InternalDOF.g:262:1: ruleNotMappedCause : ( ( rule__NotMappedCause__Group__0 ) ) ;
    public final void ruleNotMappedCause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:266:2: ( ( ( rule__NotMappedCause__Group__0 ) ) )
            // InternalDOF.g:267:2: ( ( rule__NotMappedCause__Group__0 ) )
            {
            // InternalDOF.g:267:2: ( ( rule__NotMappedCause__Group__0 ) )
            // InternalDOF.g:268:3: ( rule__NotMappedCause__Group__0 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getGroup()); 
            // InternalDOF.g:269:3: ( rule__NotMappedCause__Group__0 )
            // InternalDOF.g:269:4: rule__NotMappedCause__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleNotMappedCause"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalDOF.g:278:1: entryRuleQualifiedNameWithWildcard : ruleQualifiedNameWithWildcard EOF ;
    public final void entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        try {
            // InternalDOF.g:279:1: ( ruleQualifiedNameWithWildcard EOF )
            // InternalDOF.g:280:1: ruleQualifiedNameWithWildcard EOF
            {
             before(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalDOF.g:287:1: ruleQualifiedNameWithWildcard : ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) ;
    public final void ruleQualifiedNameWithWildcard() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:291:2: ( ( ( rule__QualifiedNameWithWildcard__Group__0 ) ) )
            // InternalDOF.g:292:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            {
            // InternalDOF.g:292:2: ( ( rule__QualifiedNameWithWildcard__Group__0 ) )
            // InternalDOF.g:293:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 
            // InternalDOF.g:294:3: ( rule__QualifiedNameWithWildcard__Group__0 )
            // InternalDOF.g:294:4: rule__QualifiedNameWithWildcard__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalDOF.g:303:1: entryRuleQualifiedName : ruleQualifiedName EOF ;
    public final void entryRuleQualifiedName() throws RecognitionException {
        try {
            // InternalDOF.g:304:1: ( ruleQualifiedName EOF )
            // InternalDOF.g:305:1: ruleQualifiedName EOF
            {
             before(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalDOF.g:312:1: ruleQualifiedName : ( ( rule__QualifiedName__Group__0 ) ) ;
    public final void ruleQualifiedName() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:316:2: ( ( ( rule__QualifiedName__Group__0 ) ) )
            // InternalDOF.g:317:2: ( ( rule__QualifiedName__Group__0 ) )
            {
            // InternalDOF.g:317:2: ( ( rule__QualifiedName__Group__0 ) )
            // InternalDOF.g:318:3: ( rule__QualifiedName__Group__0 )
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup()); 
            // InternalDOF.g:319:3: ( rule__QualifiedName__Group__0 )
            // InternalDOF.g:319:4: rule__QualifiedName__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQualifiedNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleEString"
    // InternalDOF.g:328:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalDOF.g:329:1: ( ruleEString EOF )
            // InternalDOF.g:330:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDOF.g:337:1: ruleEString : ( RULE_STRING ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:341:2: ( ( RULE_STRING ) )
            // InternalDOF.g:342:2: ( RULE_STRING )
            {
            // InternalDOF.g:342:2: ( RULE_STRING )
            // InternalDOF.g:343:3: RULE_STRING
            {
             before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "rule__Cause__Alternatives"
    // InternalDOF.g:352:1: rule__Cause__Alternatives : ( ( ruleCompoundCause ) | ( ruleDataLinkedCause ) | ( ruleNotMappedCause ) );
    public final void rule__Cause__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:356:1: ( ( ruleCompoundCause ) | ( ruleDataLinkedCause ) | ( ruleNotMappedCause ) )
            int alt1=3;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==18) ) {
                int LA1_1 = input.LA(2);

                if ( (LA1_1==RULE_ID) ) {
                    switch ( input.LA(3) ) {
                    case 14:
                    case 25:
                        {
                        alt1=2;
                        }
                        break;
                    case 22:
                        {
                        int LA1_4 = input.LA(4);

                        if ( (LA1_4==RULE_STRING) ) {
                            switch ( input.LA(5) ) {
                            case 19:
                                {
                                alt1=1;
                                }
                                break;
                            case 23:
                                {
                                alt1=3;
                                }
                                break;
                            case 14:
                                {
                                alt1=2;
                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 1, 7, input);

                                throw nvae;
                            }

                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 1, 4, input);

                            throw nvae;
                        }
                        }
                        break;
                    case 19:
                        {
                        alt1=1;
                        }
                        break;
                    case 23:
                        {
                        alt1=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 1, 2, input);

                        throw nvae;
                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 1, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // InternalDOF.g:357:2: ( ruleCompoundCause )
                    {
                    // InternalDOF.g:357:2: ( ruleCompoundCause )
                    // InternalDOF.g:358:3: ruleCompoundCause
                    {
                     before(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleCompoundCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalDOF.g:363:2: ( ruleDataLinkedCause )
                    {
                    // InternalDOF.g:363:2: ( ruleDataLinkedCause )
                    // InternalDOF.g:364:3: ruleDataLinkedCause
                    {
                     before(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleDataLinkedCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalDOF.g:369:2: ( ruleNotMappedCause )
                    {
                    // InternalDOF.g:369:2: ( ruleNotMappedCause )
                    // InternalDOF.g:370:3: ruleNotMappedCause
                    {
                     before(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleNotMappedCause();

                    state._fsp--;

                     after(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Cause__Alternatives"


    // $ANTLR start "rule__DOF__Group__0"
    // InternalDOF.g:379:1: rule__DOF__Group__0 : rule__DOF__Group__0__Impl rule__DOF__Group__1 ;
    public final void rule__DOF__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:383:1: ( rule__DOF__Group__0__Impl rule__DOF__Group__1 )
            // InternalDOF.g:384:2: rule__DOF__Group__0__Impl rule__DOF__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__DOF__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__0"


    // $ANTLR start "rule__DOF__Group__0__Impl"
    // InternalDOF.g:391:1: rule__DOF__Group__0__Impl : ( ( rule__DOF__Group_0__0 )? ) ;
    public final void rule__DOF__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:395:1: ( ( ( rule__DOF__Group_0__0 )? ) )
            // InternalDOF.g:396:1: ( ( rule__DOF__Group_0__0 )? )
            {
            // InternalDOF.g:396:1: ( ( rule__DOF__Group_0__0 )? )
            // InternalDOF.g:397:2: ( rule__DOF__Group_0__0 )?
            {
             before(grammarAccess.getDOFAccess().getGroup_0()); 
            // InternalDOF.g:398:2: ( rule__DOF__Group_0__0 )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==11) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalDOF.g:398:3: rule__DOF__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DOF__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDOFAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__0__Impl"


    // $ANTLR start "rule__DOF__Group__1"
    // InternalDOF.g:406:1: rule__DOF__Group__1 : rule__DOF__Group__1__Impl rule__DOF__Group__2 ;
    public final void rule__DOF__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:410:1: ( rule__DOF__Group__1__Impl rule__DOF__Group__2 )
            // InternalDOF.g:411:2: rule__DOF__Group__1__Impl rule__DOF__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__DOF__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__1"


    // $ANTLR start "rule__DOF__Group__1__Impl"
    // InternalDOF.g:418:1: rule__DOF__Group__1__Impl : ( ( rule__DOF__ImportsAssignment_1 )* ) ;
    public final void rule__DOF__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:422:1: ( ( ( rule__DOF__ImportsAssignment_1 )* ) )
            // InternalDOF.g:423:1: ( ( rule__DOF__ImportsAssignment_1 )* )
            {
            // InternalDOF.g:423:1: ( ( rule__DOF__ImportsAssignment_1 )* )
            // InternalDOF.g:424:2: ( rule__DOF__ImportsAssignment_1 )*
            {
             before(grammarAccess.getDOFAccess().getImportsAssignment_1()); 
            // InternalDOF.g:425:2: ( rule__DOF__ImportsAssignment_1 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==12) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalDOF.g:425:3: rule__DOF__ImportsAssignment_1
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__DOF__ImportsAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getDOFAccess().getImportsAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__1__Impl"


    // $ANTLR start "rule__DOF__Group__2"
    // InternalDOF.g:433:1: rule__DOF__Group__2 : rule__DOF__Group__2__Impl ;
    public final void rule__DOF__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:437:1: ( rule__DOF__Group__2__Impl )
            // InternalDOF.g:438:2: rule__DOF__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DOF__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__2"


    // $ANTLR start "rule__DOF__Group__2__Impl"
    // InternalDOF.g:444:1: rule__DOF__Group__2__Impl : ( ( rule__DOF__EffectsAssignment_2 ) ) ;
    public final void rule__DOF__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:448:1: ( ( ( rule__DOF__EffectsAssignment_2 ) ) )
            // InternalDOF.g:449:1: ( ( rule__DOF__EffectsAssignment_2 ) )
            {
            // InternalDOF.g:449:1: ( ( rule__DOF__EffectsAssignment_2 ) )
            // InternalDOF.g:450:2: ( rule__DOF__EffectsAssignment_2 )
            {
             before(grammarAccess.getDOFAccess().getEffectsAssignment_2()); 
            // InternalDOF.g:451:2: ( rule__DOF__EffectsAssignment_2 )
            // InternalDOF.g:451:3: rule__DOF__EffectsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__DOF__EffectsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getEffectsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group__2__Impl"


    // $ANTLR start "rule__DOF__Group_0__0"
    // InternalDOF.g:460:1: rule__DOF__Group_0__0 : rule__DOF__Group_0__0__Impl rule__DOF__Group_0__1 ;
    public final void rule__DOF__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:464:1: ( rule__DOF__Group_0__0__Impl rule__DOF__Group_0__1 )
            // InternalDOF.g:465:2: rule__DOF__Group_0__0__Impl rule__DOF__Group_0__1
            {
            pushFollow(FOLLOW_5);
            rule__DOF__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DOF__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group_0__0"


    // $ANTLR start "rule__DOF__Group_0__0__Impl"
    // InternalDOF.g:472:1: rule__DOF__Group_0__0__Impl : ( 'dof' ) ;
    public final void rule__DOF__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:476:1: ( ( 'dof' ) )
            // InternalDOF.g:477:1: ( 'dof' )
            {
            // InternalDOF.g:477:1: ( 'dof' )
            // InternalDOF.g:478:2: 'dof'
            {
             before(grammarAccess.getDOFAccess().getDofKeyword_0_0()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getDOFAccess().getDofKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group_0__0__Impl"


    // $ANTLR start "rule__DOF__Group_0__1"
    // InternalDOF.g:487:1: rule__DOF__Group_0__1 : rule__DOF__Group_0__1__Impl ;
    public final void rule__DOF__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:491:1: ( rule__DOF__Group_0__1__Impl )
            // InternalDOF.g:492:2: rule__DOF__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DOF__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group_0__1"


    // $ANTLR start "rule__DOF__Group_0__1__Impl"
    // InternalDOF.g:498:1: rule__DOF__Group_0__1__Impl : ( ( rule__DOF__NameAssignment_0_1 ) ) ;
    public final void rule__DOF__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:502:1: ( ( ( rule__DOF__NameAssignment_0_1 ) ) )
            // InternalDOF.g:503:1: ( ( rule__DOF__NameAssignment_0_1 ) )
            {
            // InternalDOF.g:503:1: ( ( rule__DOF__NameAssignment_0_1 ) )
            // InternalDOF.g:504:2: ( rule__DOF__NameAssignment_0_1 )
            {
             before(grammarAccess.getDOFAccess().getNameAssignment_0_1()); 
            // InternalDOF.g:505:2: ( rule__DOF__NameAssignment_0_1 )
            // InternalDOF.g:505:3: rule__DOF__NameAssignment_0_1
            {
            pushFollow(FOLLOW_2);
            rule__DOF__NameAssignment_0_1();

            state._fsp--;


            }

             after(grammarAccess.getDOFAccess().getNameAssignment_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__Group_0__1__Impl"


    // $ANTLR start "rule__Import__Group__0"
    // InternalDOF.g:514:1: rule__Import__Group__0 : rule__Import__Group__0__Impl rule__Import__Group__1 ;
    public final void rule__Import__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:518:1: ( rule__Import__Group__0__Impl rule__Import__Group__1 )
            // InternalDOF.g:519:2: rule__Import__Group__0__Impl rule__Import__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Import__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Import__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0"


    // $ANTLR start "rule__Import__Group__0__Impl"
    // InternalDOF.g:526:1: rule__Import__Group__0__Impl : ( 'import' ) ;
    public final void rule__Import__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:530:1: ( ( 'import' ) )
            // InternalDOF.g:531:1: ( 'import' )
            {
            // InternalDOF.g:531:1: ( 'import' )
            // InternalDOF.g:532:2: 'import'
            {
             before(grammarAccess.getImportAccess().getImportKeyword_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getImportAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__0__Impl"


    // $ANTLR start "rule__Import__Group__1"
    // InternalDOF.g:541:1: rule__Import__Group__1 : rule__Import__Group__1__Impl ;
    public final void rule__Import__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:545:1: ( rule__Import__Group__1__Impl )
            // InternalDOF.g:546:2: rule__Import__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Import__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1"


    // $ANTLR start "rule__Import__Group__1__Impl"
    // InternalDOF.g:552:1: rule__Import__Group__1__Impl : ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) ;
    public final void rule__Import__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:556:1: ( ( ( rule__Import__ImportedNamespaceAssignment_1 ) ) )
            // InternalDOF.g:557:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            {
            // InternalDOF.g:557:1: ( ( rule__Import__ImportedNamespaceAssignment_1 ) )
            // InternalDOF.g:558:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 
            // InternalDOF.g:559:2: ( rule__Import__ImportedNamespaceAssignment_1 )
            // InternalDOF.g:559:3: rule__Import__ImportedNamespaceAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Import__ImportedNamespaceAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportAccess().getImportedNamespaceAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__Group__1__Impl"


    // $ANTLR start "rule__Effect__Group__0"
    // InternalDOF.g:568:1: rule__Effect__Group__0 : rule__Effect__Group__0__Impl rule__Effect__Group__1 ;
    public final void rule__Effect__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:572:1: ( rule__Effect__Group__0__Impl rule__Effect__Group__1 )
            // InternalDOF.g:573:2: rule__Effect__Group__0__Impl rule__Effect__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Effect__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0"


    // $ANTLR start "rule__Effect__Group__0__Impl"
    // InternalDOF.g:580:1: rule__Effect__Group__0__Impl : ( 'effect' ) ;
    public final void rule__Effect__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:584:1: ( ( 'effect' ) )
            // InternalDOF.g:585:1: ( 'effect' )
            {
            // InternalDOF.g:585:1: ( 'effect' )
            // InternalDOF.g:586:2: 'effect'
            {
             before(grammarAccess.getEffectAccess().getEffectKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getEffectKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__0__Impl"


    // $ANTLR start "rule__Effect__Group__1"
    // InternalDOF.g:595:1: rule__Effect__Group__1 : rule__Effect__Group__1__Impl rule__Effect__Group__2 ;
    public final void rule__Effect__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:599:1: ( rule__Effect__Group__1__Impl rule__Effect__Group__2 )
            // InternalDOF.g:600:2: rule__Effect__Group__1__Impl rule__Effect__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__Effect__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1"


    // $ANTLR start "rule__Effect__Group__1__Impl"
    // InternalDOF.g:607:1: rule__Effect__Group__1__Impl : ( ( rule__Effect__NameAssignment_1 ) ) ;
    public final void rule__Effect__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:611:1: ( ( ( rule__Effect__NameAssignment_1 ) ) )
            // InternalDOF.g:612:1: ( ( rule__Effect__NameAssignment_1 ) )
            {
            // InternalDOF.g:612:1: ( ( rule__Effect__NameAssignment_1 ) )
            // InternalDOF.g:613:2: ( rule__Effect__NameAssignment_1 )
            {
             before(grammarAccess.getEffectAccess().getNameAssignment_1()); 
            // InternalDOF.g:614:2: ( rule__Effect__NameAssignment_1 )
            // InternalDOF.g:614:3: rule__Effect__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Effect__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__1__Impl"


    // $ANTLR start "rule__Effect__Group__2"
    // InternalDOF.g:622:1: rule__Effect__Group__2 : rule__Effect__Group__2__Impl rule__Effect__Group__3 ;
    public final void rule__Effect__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:626:1: ( rule__Effect__Group__2__Impl rule__Effect__Group__3 )
            // InternalDOF.g:627:2: rule__Effect__Group__2__Impl rule__Effect__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__Effect__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2"


    // $ANTLR start "rule__Effect__Group__2__Impl"
    // InternalDOF.g:634:1: rule__Effect__Group__2__Impl : ( 'is' ) ;
    public final void rule__Effect__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:638:1: ( ( 'is' ) )
            // InternalDOF.g:639:1: ( 'is' )
            {
            // InternalDOF.g:639:1: ( 'is' )
            // InternalDOF.g:640:2: 'is'
            {
             before(grammarAccess.getEffectAccess().getIsKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getIsKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__2__Impl"


    // $ANTLR start "rule__Effect__Group__3"
    // InternalDOF.g:649:1: rule__Effect__Group__3 : rule__Effect__Group__3__Impl rule__Effect__Group__4 ;
    public final void rule__Effect__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:653:1: ( rule__Effect__Group__3__Impl rule__Effect__Group__4 )
            // InternalDOF.g:654:2: rule__Effect__Group__3__Impl rule__Effect__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__Effect__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3"


    // $ANTLR start "rule__Effect__Group__3__Impl"
    // InternalDOF.g:661:1: rule__Effect__Group__3__Impl : ( ( rule__Effect__DataFeederAssignment_3 ) ) ;
    public final void rule__Effect__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:665:1: ( ( ( rule__Effect__DataFeederAssignment_3 ) ) )
            // InternalDOF.g:666:1: ( ( rule__Effect__DataFeederAssignment_3 ) )
            {
            // InternalDOF.g:666:1: ( ( rule__Effect__DataFeederAssignment_3 ) )
            // InternalDOF.g:667:2: ( rule__Effect__DataFeederAssignment_3 )
            {
             before(grammarAccess.getEffectAccess().getDataFeederAssignment_3()); 
            // InternalDOF.g:668:2: ( rule__Effect__DataFeederAssignment_3 )
            // InternalDOF.g:668:3: rule__Effect__DataFeederAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Effect__DataFeederAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getDataFeederAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__3__Impl"


    // $ANTLR start "rule__Effect__Group__4"
    // InternalDOF.g:676:1: rule__Effect__Group__4 : rule__Effect__Group__4__Impl rule__Effect__Group__5 ;
    public final void rule__Effect__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:680:1: ( rule__Effect__Group__4__Impl rule__Effect__Group__5 )
            // InternalDOF.g:681:2: rule__Effect__Group__4__Impl rule__Effect__Group__5
            {
            pushFollow(FOLLOW_8);
            rule__Effect__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4"


    // $ANTLR start "rule__Effect__Group__4__Impl"
    // InternalDOF.g:688:1: rule__Effect__Group__4__Impl : ( 'include' ) ;
    public final void rule__Effect__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:692:1: ( ( 'include' ) )
            // InternalDOF.g:693:1: ( 'include' )
            {
            // InternalDOF.g:693:1: ( 'include' )
            // InternalDOF.g:694:2: 'include'
            {
             before(grammarAccess.getEffectAccess().getIncludeKeyword_4()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getIncludeKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__4__Impl"


    // $ANTLR start "rule__Effect__Group__5"
    // InternalDOF.g:703:1: rule__Effect__Group__5 : rule__Effect__Group__5__Impl rule__Effect__Group__6 ;
    public final void rule__Effect__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:707:1: ( rule__Effect__Group__5__Impl rule__Effect__Group__6 )
            // InternalDOF.g:708:2: rule__Effect__Group__5__Impl rule__Effect__Group__6
            {
            pushFollow(FOLLOW_8);
            rule__Effect__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Effect__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__5"


    // $ANTLR start "rule__Effect__Group__5__Impl"
    // InternalDOF.g:715:1: rule__Effect__Group__5__Impl : ( ( rule__Effect__CategoriesAssignment_5 ) ) ;
    public final void rule__Effect__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:719:1: ( ( ( rule__Effect__CategoriesAssignment_5 ) ) )
            // InternalDOF.g:720:1: ( ( rule__Effect__CategoriesAssignment_5 ) )
            {
            // InternalDOF.g:720:1: ( ( rule__Effect__CategoriesAssignment_5 ) )
            // InternalDOF.g:721:2: ( rule__Effect__CategoriesAssignment_5 )
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_5()); 
            // InternalDOF.g:722:2: ( rule__Effect__CategoriesAssignment_5 )
            // InternalDOF.g:722:3: rule__Effect__CategoriesAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__Effect__CategoriesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__5__Impl"


    // $ANTLR start "rule__Effect__Group__6"
    // InternalDOF.g:730:1: rule__Effect__Group__6 : rule__Effect__Group__6__Impl ;
    public final void rule__Effect__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:734:1: ( rule__Effect__Group__6__Impl )
            // InternalDOF.g:735:2: rule__Effect__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Effect__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__6"


    // $ANTLR start "rule__Effect__Group__6__Impl"
    // InternalDOF.g:741:1: rule__Effect__Group__6__Impl : ( ( rule__Effect__CategoriesAssignment_6 )* ) ;
    public final void rule__Effect__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:745:1: ( ( ( rule__Effect__CategoriesAssignment_6 )* ) )
            // InternalDOF.g:746:1: ( ( rule__Effect__CategoriesAssignment_6 )* )
            {
            // InternalDOF.g:746:1: ( ( rule__Effect__CategoriesAssignment_6 )* )
            // InternalDOF.g:747:2: ( rule__Effect__CategoriesAssignment_6 )*
            {
             before(grammarAccess.getEffectAccess().getCategoriesAssignment_6()); 
            // InternalDOF.g:748:2: ( rule__Effect__CategoriesAssignment_6 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==16) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalDOF.g:748:3: rule__Effect__CategoriesAssignment_6
            	    {
            	    pushFollow(FOLLOW_9);
            	    rule__Effect__CategoriesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getEffectAccess().getCategoriesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__Group__6__Impl"


    // $ANTLR start "rule__Category__Group__0"
    // InternalDOF.g:757:1: rule__Category__Group__0 : rule__Category__Group__0__Impl rule__Category__Group__1 ;
    public final void rule__Category__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:761:1: ( rule__Category__Group__0__Impl rule__Category__Group__1 )
            // InternalDOF.g:762:2: rule__Category__Group__0__Impl rule__Category__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Category__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0"


    // $ANTLR start "rule__Category__Group__0__Impl"
    // InternalDOF.g:769:1: rule__Category__Group__0__Impl : ( 'category' ) ;
    public final void rule__Category__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:773:1: ( ( 'category' ) )
            // InternalDOF.g:774:1: ( 'category' )
            {
            // InternalDOF.g:774:1: ( 'category' )
            // InternalDOF.g:775:2: 'category'
            {
             before(grammarAccess.getCategoryAccess().getCategoryKeyword_0()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getCategoryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__0__Impl"


    // $ANTLR start "rule__Category__Group__1"
    // InternalDOF.g:784:1: rule__Category__Group__1 : rule__Category__Group__1__Impl rule__Category__Group__2 ;
    public final void rule__Category__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:788:1: ( rule__Category__Group__1__Impl rule__Category__Group__2 )
            // InternalDOF.g:789:2: rule__Category__Group__1__Impl rule__Category__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__Category__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1"


    // $ANTLR start "rule__Category__Group__1__Impl"
    // InternalDOF.g:796:1: rule__Category__Group__1__Impl : ( ( rule__Category__NameAssignment_1 ) ) ;
    public final void rule__Category__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:800:1: ( ( ( rule__Category__NameAssignment_1 ) ) )
            // InternalDOF.g:801:1: ( ( rule__Category__NameAssignment_1 ) )
            {
            // InternalDOF.g:801:1: ( ( rule__Category__NameAssignment_1 ) )
            // InternalDOF.g:802:2: ( rule__Category__NameAssignment_1 )
            {
             before(grammarAccess.getCategoryAccess().getNameAssignment_1()); 
            // InternalDOF.g:803:2: ( rule__Category__NameAssignment_1 )
            // InternalDOF.g:803:3: rule__Category__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Category__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__1__Impl"


    // $ANTLR start "rule__Category__Group__2"
    // InternalDOF.g:811:1: rule__Category__Group__2 : rule__Category__Group__2__Impl rule__Category__Group__3 ;
    public final void rule__Category__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:815:1: ( rule__Category__Group__2__Impl rule__Category__Group__3 )
            // InternalDOF.g:816:2: rule__Category__Group__2__Impl rule__Category__Group__3
            {
            pushFollow(FOLLOW_10);
            rule__Category__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2"


    // $ANTLR start "rule__Category__Group__2__Impl"
    // InternalDOF.g:823:1: rule__Category__Group__2__Impl : ( ( rule__Category__Group_2__0 )? ) ;
    public final void rule__Category__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:827:1: ( ( ( rule__Category__Group_2__0 )? ) )
            // InternalDOF.g:828:1: ( ( rule__Category__Group_2__0 )? )
            {
            // InternalDOF.g:828:1: ( ( rule__Category__Group_2__0 )? )
            // InternalDOF.g:829:2: ( rule__Category__Group_2__0 )?
            {
             before(grammarAccess.getCategoryAccess().getGroup_2()); 
            // InternalDOF.g:830:2: ( rule__Category__Group_2__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==17) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalDOF.g:830:3: rule__Category__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Category__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCategoryAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__2__Impl"


    // $ANTLR start "rule__Category__Group__3"
    // InternalDOF.g:838:1: rule__Category__Group__3 : rule__Category__Group__3__Impl rule__Category__Group__4 ;
    public final void rule__Category__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:842:1: ( rule__Category__Group__3__Impl rule__Category__Group__4 )
            // InternalDOF.g:843:2: rule__Category__Group__3__Impl rule__Category__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__Category__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3"


    // $ANTLR start "rule__Category__Group__3__Impl"
    // InternalDOF.g:850:1: rule__Category__Group__3__Impl : ( ( rule__Category__CausesAssignment_3 ) ) ;
    public final void rule__Category__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:854:1: ( ( ( rule__Category__CausesAssignment_3 ) ) )
            // InternalDOF.g:855:1: ( ( rule__Category__CausesAssignment_3 ) )
            {
            // InternalDOF.g:855:1: ( ( rule__Category__CausesAssignment_3 ) )
            // InternalDOF.g:856:2: ( rule__Category__CausesAssignment_3 )
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_3()); 
            // InternalDOF.g:857:2: ( rule__Category__CausesAssignment_3 )
            // InternalDOF.g:857:3: rule__Category__CausesAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Category__CausesAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getCausesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__3__Impl"


    // $ANTLR start "rule__Category__Group__4"
    // InternalDOF.g:865:1: rule__Category__Group__4 : rule__Category__Group__4__Impl ;
    public final void rule__Category__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:869:1: ( rule__Category__Group__4__Impl )
            // InternalDOF.g:870:2: rule__Category__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4"


    // $ANTLR start "rule__Category__Group__4__Impl"
    // InternalDOF.g:876:1: rule__Category__Group__4__Impl : ( ( rule__Category__CausesAssignment_4 )* ) ;
    public final void rule__Category__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:880:1: ( ( ( rule__Category__CausesAssignment_4 )* ) )
            // InternalDOF.g:881:1: ( ( rule__Category__CausesAssignment_4 )* )
            {
            // InternalDOF.g:881:1: ( ( rule__Category__CausesAssignment_4 )* )
            // InternalDOF.g:882:2: ( rule__Category__CausesAssignment_4 )*
            {
             before(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 
            // InternalDOF.g:883:2: ( rule__Category__CausesAssignment_4 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==18) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalDOF.g:883:3: rule__Category__CausesAssignment_4
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__Category__CausesAssignment_4();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getCategoryAccess().getCausesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group__4__Impl"


    // $ANTLR start "rule__Category__Group_2__0"
    // InternalDOF.g:892:1: rule__Category__Group_2__0 : rule__Category__Group_2__0__Impl rule__Category__Group_2__1 ;
    public final void rule__Category__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:896:1: ( rule__Category__Group_2__0__Impl rule__Category__Group_2__1 )
            // InternalDOF.g:897:2: rule__Category__Group_2__0__Impl rule__Category__Group_2__1
            {
            pushFollow(FOLLOW_5);
            rule__Category__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Category__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_2__0"


    // $ANTLR start "rule__Category__Group_2__0__Impl"
    // InternalDOF.g:904:1: rule__Category__Group_2__0__Impl : ( 'otras' ) ;
    public final void rule__Category__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:908:1: ( ( 'otras' ) )
            // InternalDOF.g:909:1: ( 'otras' )
            {
            // InternalDOF.g:909:1: ( 'otras' )
            // InternalDOF.g:910:2: 'otras'
            {
             before(grammarAccess.getCategoryAccess().getOtrasKeyword_2_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getOtrasKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_2__0__Impl"


    // $ANTLR start "rule__Category__Group_2__1"
    // InternalDOF.g:919:1: rule__Category__Group_2__1 : rule__Category__Group_2__1__Impl ;
    public final void rule__Category__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:923:1: ( rule__Category__Group_2__1__Impl )
            // InternalDOF.g:924:2: rule__Category__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Category__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_2__1"


    // $ANTLR start "rule__Category__Group_2__1__Impl"
    // InternalDOF.g:930:1: rule__Category__Group_2__1__Impl : ( ( rule__Category__TypeAssignment_2_1 ) ) ;
    public final void rule__Category__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:934:1: ( ( ( rule__Category__TypeAssignment_2_1 ) ) )
            // InternalDOF.g:935:1: ( ( rule__Category__TypeAssignment_2_1 ) )
            {
            // InternalDOF.g:935:1: ( ( rule__Category__TypeAssignment_2_1 ) )
            // InternalDOF.g:936:2: ( rule__Category__TypeAssignment_2_1 )
            {
             before(grammarAccess.getCategoryAccess().getTypeAssignment_2_1()); 
            // InternalDOF.g:937:2: ( rule__Category__TypeAssignment_2_1 )
            // InternalDOF.g:937:3: rule__Category__TypeAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Category__TypeAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getCategoryAccess().getTypeAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__Group_2__1__Impl"


    // $ANTLR start "rule__DataFeeder__Group__0"
    // InternalDOF.g:946:1: rule__DataFeeder__Group__0 : rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1 ;
    public final void rule__DataFeeder__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:950:1: ( rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1 )
            // InternalDOF.g:951:2: rule__DataFeeder__Group__0__Impl rule__DataFeeder__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__DataFeeder__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__0"


    // $ANTLR start "rule__DataFeeder__Group__0__Impl"
    // InternalDOF.g:958:1: rule__DataFeeder__Group__0__Impl : ( () ) ;
    public final void rule__DataFeeder__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:962:1: ( ( () ) )
            // InternalDOF.g:963:1: ( () )
            {
            // InternalDOF.g:963:1: ( () )
            // InternalDOF.g:964:2: ()
            {
             before(grammarAccess.getDataFeederAccess().getDataFeederAction_0()); 
            // InternalDOF.g:965:2: ()
            // InternalDOF.g:965:3: 
            {
            }

             after(grammarAccess.getDataFeederAccess().getDataFeederAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__0__Impl"


    // $ANTLR start "rule__DataFeeder__Group__1"
    // InternalDOF.g:973:1: rule__DataFeeder__Group__1 : rule__DataFeeder__Group__1__Impl ;
    public final void rule__DataFeeder__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:977:1: ( rule__DataFeeder__Group__1__Impl )
            // InternalDOF.g:978:2: rule__DataFeeder__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__1"


    // $ANTLR start "rule__DataFeeder__Group__1__Impl"
    // InternalDOF.g:984:1: rule__DataFeeder__Group__1__Impl : ( ( rule__DataFeeder__NameAssignment_1 ) ) ;
    public final void rule__DataFeeder__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:988:1: ( ( ( rule__DataFeeder__NameAssignment_1 ) ) )
            // InternalDOF.g:989:1: ( ( rule__DataFeeder__NameAssignment_1 ) )
            {
            // InternalDOF.g:989:1: ( ( rule__DataFeeder__NameAssignment_1 ) )
            // InternalDOF.g:990:2: ( rule__DataFeeder__NameAssignment_1 )
            {
             before(grammarAccess.getDataFeederAccess().getNameAssignment_1()); 
            // InternalDOF.g:991:2: ( rule__DataFeeder__NameAssignment_1 )
            // InternalDOF.g:991:3: rule__DataFeeder__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DataFeeder__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataFeederAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__Group__1__Impl"


    // $ANTLR start "rule__CompoundCause__Group__0"
    // InternalDOF.g:1000:1: rule__CompoundCause__Group__0 : rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1 ;
    public final void rule__CompoundCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1004:1: ( rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1 )
            // InternalDOF.g:1005:2: rule__CompoundCause__Group__0__Impl rule__CompoundCause__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__CompoundCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__0"


    // $ANTLR start "rule__CompoundCause__Group__0__Impl"
    // InternalDOF.g:1012:1: rule__CompoundCause__Group__0__Impl : ( 'cause' ) ;
    public final void rule__CompoundCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1016:1: ( ( 'cause' ) )
            // InternalDOF.g:1017:1: ( 'cause' )
            {
            // InternalDOF.g:1017:1: ( 'cause' )
            // InternalDOF.g:1018:2: 'cause'
            {
             before(grammarAccess.getCompoundCauseAccess().getCauseKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getCauseKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__0__Impl"


    // $ANTLR start "rule__CompoundCause__Group__1"
    // InternalDOF.g:1027:1: rule__CompoundCause__Group__1 : rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2 ;
    public final void rule__CompoundCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1031:1: ( rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2 )
            // InternalDOF.g:1032:2: rule__CompoundCause__Group__1__Impl rule__CompoundCause__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__CompoundCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__1"


    // $ANTLR start "rule__CompoundCause__Group__1__Impl"
    // InternalDOF.g:1039:1: rule__CompoundCause__Group__1__Impl : ( ( rule__CompoundCause__NameAssignment_1 ) ) ;
    public final void rule__CompoundCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1043:1: ( ( ( rule__CompoundCause__NameAssignment_1 ) ) )
            // InternalDOF.g:1044:1: ( ( rule__CompoundCause__NameAssignment_1 ) )
            {
            // InternalDOF.g:1044:1: ( ( rule__CompoundCause__NameAssignment_1 ) )
            // InternalDOF.g:1045:2: ( rule__CompoundCause__NameAssignment_1 )
            {
             before(grammarAccess.getCompoundCauseAccess().getNameAssignment_1()); 
            // InternalDOF.g:1046:2: ( rule__CompoundCause__NameAssignment_1 )
            // InternalDOF.g:1046:3: rule__CompoundCause__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__1__Impl"


    // $ANTLR start "rule__CompoundCause__Group__2"
    // InternalDOF.g:1054:1: rule__CompoundCause__Group__2 : rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3 ;
    public final void rule__CompoundCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1058:1: ( rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3 )
            // InternalDOF.g:1059:2: rule__CompoundCause__Group__2__Impl rule__CompoundCause__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__CompoundCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__2"


    // $ANTLR start "rule__CompoundCause__Group__2__Impl"
    // InternalDOF.g:1066:1: rule__CompoundCause__Group__2__Impl : ( ( rule__CompoundCause__Group_2__0 )? ) ;
    public final void rule__CompoundCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1070:1: ( ( ( rule__CompoundCause__Group_2__0 )? ) )
            // InternalDOF.g:1071:1: ( ( rule__CompoundCause__Group_2__0 )? )
            {
            // InternalDOF.g:1071:1: ( ( rule__CompoundCause__Group_2__0 )? )
            // InternalDOF.g:1072:2: ( rule__CompoundCause__Group_2__0 )?
            {
             before(grammarAccess.getCompoundCauseAccess().getGroup_2()); 
            // InternalDOF.g:1073:2: ( rule__CompoundCause__Group_2__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==22) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalDOF.g:1073:3: rule__CompoundCause__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__CompoundCause__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getCompoundCauseAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__2__Impl"


    // $ANTLR start "rule__CompoundCause__Group__3"
    // InternalDOF.g:1081:1: rule__CompoundCause__Group__3 : rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4 ;
    public final void rule__CompoundCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1085:1: ( rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4 )
            // InternalDOF.g:1086:2: rule__CompoundCause__Group__3__Impl rule__CompoundCause__Group__4
            {
            pushFollow(FOLLOW_13);
            rule__CompoundCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__3"


    // $ANTLR start "rule__CompoundCause__Group__3__Impl"
    // InternalDOF.g:1093:1: rule__CompoundCause__Group__3__Impl : ( 'contains' ) ;
    public final void rule__CompoundCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1097:1: ( ( 'contains' ) )
            // InternalDOF.g:1098:1: ( 'contains' )
            {
            // InternalDOF.g:1098:1: ( 'contains' )
            // InternalDOF.g:1099:2: 'contains'
            {
             before(grammarAccess.getCompoundCauseAccess().getContainsKeyword_3()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getContainsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__3__Impl"


    // $ANTLR start "rule__CompoundCause__Group__4"
    // InternalDOF.g:1108:1: rule__CompoundCause__Group__4 : rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5 ;
    public final void rule__CompoundCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1112:1: ( rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5 )
            // InternalDOF.g:1113:2: rule__CompoundCause__Group__4__Impl rule__CompoundCause__Group__5
            {
            pushFollow(FOLLOW_10);
            rule__CompoundCause__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__4"


    // $ANTLR start "rule__CompoundCause__Group__4__Impl"
    // InternalDOF.g:1120:1: rule__CompoundCause__Group__4__Impl : ( '{' ) ;
    public final void rule__CompoundCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1124:1: ( ( '{' ) )
            // InternalDOF.g:1125:1: ( '{' )
            {
            // InternalDOF.g:1125:1: ( '{' )
            // InternalDOF.g:1126:2: '{'
            {
             before(grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__4__Impl"


    // $ANTLR start "rule__CompoundCause__Group__5"
    // InternalDOF.g:1135:1: rule__CompoundCause__Group__5 : rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6 ;
    public final void rule__CompoundCause__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1139:1: ( rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6 )
            // InternalDOF.g:1140:2: rule__CompoundCause__Group__5__Impl rule__CompoundCause__Group__6
            {
            pushFollow(FOLLOW_14);
            rule__CompoundCause__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__5"


    // $ANTLR start "rule__CompoundCause__Group__5__Impl"
    // InternalDOF.g:1147:1: rule__CompoundCause__Group__5__Impl : ( ( rule__CompoundCause__SubCausesAssignment_5 ) ) ;
    public final void rule__CompoundCause__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1151:1: ( ( ( rule__CompoundCause__SubCausesAssignment_5 ) ) )
            // InternalDOF.g:1152:1: ( ( rule__CompoundCause__SubCausesAssignment_5 ) )
            {
            // InternalDOF.g:1152:1: ( ( rule__CompoundCause__SubCausesAssignment_5 ) )
            // InternalDOF.g:1153:2: ( rule__CompoundCause__SubCausesAssignment_5 )
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_5()); 
            // InternalDOF.g:1154:2: ( rule__CompoundCause__SubCausesAssignment_5 )
            // InternalDOF.g:1154:3: rule__CompoundCause__SubCausesAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__SubCausesAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__5__Impl"


    // $ANTLR start "rule__CompoundCause__Group__6"
    // InternalDOF.g:1162:1: rule__CompoundCause__Group__6 : rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7 ;
    public final void rule__CompoundCause__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1166:1: ( rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7 )
            // InternalDOF.g:1167:2: rule__CompoundCause__Group__6__Impl rule__CompoundCause__Group__7
            {
            pushFollow(FOLLOW_14);
            rule__CompoundCause__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__6"


    // $ANTLR start "rule__CompoundCause__Group__6__Impl"
    // InternalDOF.g:1174:1: rule__CompoundCause__Group__6__Impl : ( ( rule__CompoundCause__SubCausesAssignment_6 )* ) ;
    public final void rule__CompoundCause__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1178:1: ( ( ( rule__CompoundCause__SubCausesAssignment_6 )* ) )
            // InternalDOF.g:1179:1: ( ( rule__CompoundCause__SubCausesAssignment_6 )* )
            {
            // InternalDOF.g:1179:1: ( ( rule__CompoundCause__SubCausesAssignment_6 )* )
            // InternalDOF.g:1180:2: ( rule__CompoundCause__SubCausesAssignment_6 )*
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_6()); 
            // InternalDOF.g:1181:2: ( rule__CompoundCause__SubCausesAssignment_6 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==18) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalDOF.g:1181:3: rule__CompoundCause__SubCausesAssignment_6
            	    {
            	    pushFollow(FOLLOW_11);
            	    rule__CompoundCause__SubCausesAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getCompoundCauseAccess().getSubCausesAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__6__Impl"


    // $ANTLR start "rule__CompoundCause__Group__7"
    // InternalDOF.g:1189:1: rule__CompoundCause__Group__7 : rule__CompoundCause__Group__7__Impl ;
    public final void rule__CompoundCause__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1193:1: ( rule__CompoundCause__Group__7__Impl )
            // InternalDOF.g:1194:2: rule__CompoundCause__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__7"


    // $ANTLR start "rule__CompoundCause__Group__7__Impl"
    // InternalDOF.g:1200:1: rule__CompoundCause__Group__7__Impl : ( '}' ) ;
    public final void rule__CompoundCause__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1204:1: ( ( '}' ) )
            // InternalDOF.g:1205:1: ( '}' )
            {
            // InternalDOF.g:1205:1: ( '}' )
            // InternalDOF.g:1206:2: '}'
            {
             before(grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_7()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group__7__Impl"


    // $ANTLR start "rule__CompoundCause__Group_2__0"
    // InternalDOF.g:1216:1: rule__CompoundCause__Group_2__0 : rule__CompoundCause__Group_2__0__Impl rule__CompoundCause__Group_2__1 ;
    public final void rule__CompoundCause__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1220:1: ( rule__CompoundCause__Group_2__0__Impl rule__CompoundCause__Group_2__1 )
            // InternalDOF.g:1221:2: rule__CompoundCause__Group_2__0__Impl rule__CompoundCause__Group_2__1
            {
            pushFollow(FOLLOW_15);
            rule__CompoundCause__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_2__0"


    // $ANTLR start "rule__CompoundCause__Group_2__0__Impl"
    // InternalDOF.g:1228:1: rule__CompoundCause__Group_2__0__Impl : ( 'realizes' ) ;
    public final void rule__CompoundCause__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1232:1: ( ( 'realizes' ) )
            // InternalDOF.g:1233:1: ( 'realizes' )
            {
            // InternalDOF.g:1233:1: ( 'realizes' )
            // InternalDOF.g:1234:2: 'realizes'
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesKeyword_2_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getRealizesKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_2__0__Impl"


    // $ANTLR start "rule__CompoundCause__Group_2__1"
    // InternalDOF.g:1243:1: rule__CompoundCause__Group_2__1 : rule__CompoundCause__Group_2__1__Impl ;
    public final void rule__CompoundCause__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1247:1: ( rule__CompoundCause__Group_2__1__Impl )
            // InternalDOF.g:1248:2: rule__CompoundCause__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_2__1"


    // $ANTLR start "rule__CompoundCause__Group_2__1__Impl"
    // InternalDOF.g:1254:1: rule__CompoundCause__Group_2__1__Impl : ( ( rule__CompoundCause__RealizesAssignment_2_1 ) ) ;
    public final void rule__CompoundCause__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1258:1: ( ( ( rule__CompoundCause__RealizesAssignment_2_1 ) ) )
            // InternalDOF.g:1259:1: ( ( rule__CompoundCause__RealizesAssignment_2_1 ) )
            {
            // InternalDOF.g:1259:1: ( ( rule__CompoundCause__RealizesAssignment_2_1 ) )
            // InternalDOF.g:1260:2: ( rule__CompoundCause__RealizesAssignment_2_1 )
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesAssignment_2_1()); 
            // InternalDOF.g:1261:2: ( rule__CompoundCause__RealizesAssignment_2_1 )
            // InternalDOF.g:1261:3: rule__CompoundCause__RealizesAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__CompoundCause__RealizesAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getCompoundCauseAccess().getRealizesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__Group_2__1__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__0"
    // InternalDOF.g:1270:1: rule__DataLinkedCause__Group__0 : rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1 ;
    public final void rule__DataLinkedCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1274:1: ( rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1 )
            // InternalDOF.g:1275:2: rule__DataLinkedCause__Group__0__Impl rule__DataLinkedCause__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__DataLinkedCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__0"


    // $ANTLR start "rule__DataLinkedCause__Group__0__Impl"
    // InternalDOF.g:1282:1: rule__DataLinkedCause__Group__0__Impl : ( 'cause' ) ;
    public final void rule__DataLinkedCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1286:1: ( ( 'cause' ) )
            // InternalDOF.g:1287:1: ( 'cause' )
            {
            // InternalDOF.g:1287:1: ( 'cause' )
            // InternalDOF.g:1288:2: 'cause'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__0__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__1"
    // InternalDOF.g:1297:1: rule__DataLinkedCause__Group__1 : rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2 ;
    public final void rule__DataLinkedCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1301:1: ( rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2 )
            // InternalDOF.g:1302:2: rule__DataLinkedCause__Group__1__Impl rule__DataLinkedCause__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__DataLinkedCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__1"


    // $ANTLR start "rule__DataLinkedCause__Group__1__Impl"
    // InternalDOF.g:1309:1: rule__DataLinkedCause__Group__1__Impl : ( ( rule__DataLinkedCause__NameAssignment_1 ) ) ;
    public final void rule__DataLinkedCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1313:1: ( ( ( rule__DataLinkedCause__NameAssignment_1 ) ) )
            // InternalDOF.g:1314:1: ( ( rule__DataLinkedCause__NameAssignment_1 ) )
            {
            // InternalDOF.g:1314:1: ( ( rule__DataLinkedCause__NameAssignment_1 ) )
            // InternalDOF.g:1315:2: ( rule__DataLinkedCause__NameAssignment_1 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getNameAssignment_1()); 
            // InternalDOF.g:1316:2: ( rule__DataLinkedCause__NameAssignment_1 )
            // InternalDOF.g:1316:3: rule__DataLinkedCause__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__1__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__2"
    // InternalDOF.g:1324:1: rule__DataLinkedCause__Group__2 : rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3 ;
    public final void rule__DataLinkedCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1328:1: ( rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3 )
            // InternalDOF.g:1329:2: rule__DataLinkedCause__Group__2__Impl rule__DataLinkedCause__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__DataLinkedCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__2"


    // $ANTLR start "rule__DataLinkedCause__Group__2__Impl"
    // InternalDOF.g:1336:1: rule__DataLinkedCause__Group__2__Impl : ( ( rule__DataLinkedCause__Group_2__0 )? ) ;
    public final void rule__DataLinkedCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1340:1: ( ( ( rule__DataLinkedCause__Group_2__0 )? ) )
            // InternalDOF.g:1341:1: ( ( rule__DataLinkedCause__Group_2__0 )? )
            {
            // InternalDOF.g:1341:1: ( ( rule__DataLinkedCause__Group_2__0 )? )
            // InternalDOF.g:1342:2: ( rule__DataLinkedCause__Group_2__0 )?
            {
             before(grammarAccess.getDataLinkedCauseAccess().getGroup_2()); 
            // InternalDOF.g:1343:2: ( rule__DataLinkedCause__Group_2__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==22) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalDOF.g:1343:3: rule__DataLinkedCause__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__DataLinkedCause__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDataLinkedCauseAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__2__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__3"
    // InternalDOF.g:1351:1: rule__DataLinkedCause__Group__3 : rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4 ;
    public final void rule__DataLinkedCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1355:1: ( rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4 )
            // InternalDOF.g:1356:2: rule__DataLinkedCause__Group__3__Impl rule__DataLinkedCause__Group__4
            {
            pushFollow(FOLLOW_5);
            rule__DataLinkedCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__3"


    // $ANTLR start "rule__DataLinkedCause__Group__3__Impl"
    // InternalDOF.g:1363:1: rule__DataLinkedCause__Group__3__Impl : ( 'is' ) ;
    public final void rule__DataLinkedCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1367:1: ( ( 'is' ) )
            // InternalDOF.g:1368:1: ( 'is' )
            {
            // InternalDOF.g:1368:1: ( 'is' )
            // InternalDOF.g:1369:2: 'is'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getIsKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getIsKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__3__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group__4"
    // InternalDOF.g:1378:1: rule__DataLinkedCause__Group__4 : rule__DataLinkedCause__Group__4__Impl ;
    public final void rule__DataLinkedCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1382:1: ( rule__DataLinkedCause__Group__4__Impl )
            // InternalDOF.g:1383:2: rule__DataLinkedCause__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__4"


    // $ANTLR start "rule__DataLinkedCause__Group__4__Impl"
    // InternalDOF.g:1389:1: rule__DataLinkedCause__Group__4__Impl : ( ( rule__DataLinkedCause__DataFeederAssignment_4 ) ) ;
    public final void rule__DataLinkedCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1393:1: ( ( ( rule__DataLinkedCause__DataFeederAssignment_4 ) ) )
            // InternalDOF.g:1394:1: ( ( rule__DataLinkedCause__DataFeederAssignment_4 ) )
            {
            // InternalDOF.g:1394:1: ( ( rule__DataLinkedCause__DataFeederAssignment_4 ) )
            // InternalDOF.g:1395:2: ( rule__DataLinkedCause__DataFeederAssignment_4 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getDataFeederAssignment_4()); 
            // InternalDOF.g:1396:2: ( rule__DataLinkedCause__DataFeederAssignment_4 )
            // InternalDOF.g:1396:3: rule__DataLinkedCause__DataFeederAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__DataFeederAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getDataFeederAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group__4__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group_2__0"
    // InternalDOF.g:1405:1: rule__DataLinkedCause__Group_2__0 : rule__DataLinkedCause__Group_2__0__Impl rule__DataLinkedCause__Group_2__1 ;
    public final void rule__DataLinkedCause__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1409:1: ( rule__DataLinkedCause__Group_2__0__Impl rule__DataLinkedCause__Group_2__1 )
            // InternalDOF.g:1410:2: rule__DataLinkedCause__Group_2__0__Impl rule__DataLinkedCause__Group_2__1
            {
            pushFollow(FOLLOW_15);
            rule__DataLinkedCause__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_2__0"


    // $ANTLR start "rule__DataLinkedCause__Group_2__0__Impl"
    // InternalDOF.g:1417:1: rule__DataLinkedCause__Group_2__0__Impl : ( 'realizes' ) ;
    public final void rule__DataLinkedCause__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1421:1: ( ( 'realizes' ) )
            // InternalDOF.g:1422:1: ( 'realizes' )
            {
            // InternalDOF.g:1422:1: ( 'realizes' )
            // InternalDOF.g:1423:2: 'realizes'
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_2_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_2__0__Impl"


    // $ANTLR start "rule__DataLinkedCause__Group_2__1"
    // InternalDOF.g:1432:1: rule__DataLinkedCause__Group_2__1 : rule__DataLinkedCause__Group_2__1__Impl ;
    public final void rule__DataLinkedCause__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1436:1: ( rule__DataLinkedCause__Group_2__1__Impl )
            // InternalDOF.g:1437:2: rule__DataLinkedCause__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_2__1"


    // $ANTLR start "rule__DataLinkedCause__Group_2__1__Impl"
    // InternalDOF.g:1443:1: rule__DataLinkedCause__Group_2__1__Impl : ( ( rule__DataLinkedCause__RealizesAssignment_2_1 ) ) ;
    public final void rule__DataLinkedCause__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1447:1: ( ( ( rule__DataLinkedCause__RealizesAssignment_2_1 ) ) )
            // InternalDOF.g:1448:1: ( ( rule__DataLinkedCause__RealizesAssignment_2_1 ) )
            {
            // InternalDOF.g:1448:1: ( ( rule__DataLinkedCause__RealizesAssignment_2_1 ) )
            // InternalDOF.g:1449:2: ( rule__DataLinkedCause__RealizesAssignment_2_1 )
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesAssignment_2_1()); 
            // InternalDOF.g:1450:2: ( rule__DataLinkedCause__RealizesAssignment_2_1 )
            // InternalDOF.g:1450:3: rule__DataLinkedCause__RealizesAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__DataLinkedCause__RealizesAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getDataLinkedCauseAccess().getRealizesAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__Group_2__1__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__0"
    // InternalDOF.g:1459:1: rule__NotMappedCause__Group__0 : rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1 ;
    public final void rule__NotMappedCause__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1463:1: ( rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1 )
            // InternalDOF.g:1464:2: rule__NotMappedCause__Group__0__Impl rule__NotMappedCause__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__NotMappedCause__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__0"


    // $ANTLR start "rule__NotMappedCause__Group__0__Impl"
    // InternalDOF.g:1471:1: rule__NotMappedCause__Group__0__Impl : ( () ) ;
    public final void rule__NotMappedCause__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1475:1: ( ( () ) )
            // InternalDOF.g:1476:1: ( () )
            {
            // InternalDOF.g:1476:1: ( () )
            // InternalDOF.g:1477:2: ()
            {
             before(grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0()); 
            // InternalDOF.g:1478:2: ()
            // InternalDOF.g:1478:3: 
            {
            }

             after(grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__0__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__1"
    // InternalDOF.g:1486:1: rule__NotMappedCause__Group__1 : rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2 ;
    public final void rule__NotMappedCause__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1490:1: ( rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2 )
            // InternalDOF.g:1491:2: rule__NotMappedCause__Group__1__Impl rule__NotMappedCause__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__NotMappedCause__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__1"


    // $ANTLR start "rule__NotMappedCause__Group__1__Impl"
    // InternalDOF.g:1498:1: rule__NotMappedCause__Group__1__Impl : ( 'cause' ) ;
    public final void rule__NotMappedCause__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1502:1: ( ( 'cause' ) )
            // InternalDOF.g:1503:1: ( 'cause' )
            {
            // InternalDOF.g:1503:1: ( 'cause' )
            // InternalDOF.g:1504:2: 'cause'
            {
             before(grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__1__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__2"
    // InternalDOF.g:1513:1: rule__NotMappedCause__Group__2 : rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3 ;
    public final void rule__NotMappedCause__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1517:1: ( rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3 )
            // InternalDOF.g:1518:2: rule__NotMappedCause__Group__2__Impl rule__NotMappedCause__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__NotMappedCause__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__2"


    // $ANTLR start "rule__NotMappedCause__Group__2__Impl"
    // InternalDOF.g:1525:1: rule__NotMappedCause__Group__2__Impl : ( ( rule__NotMappedCause__NameAssignment_2 ) ) ;
    public final void rule__NotMappedCause__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1529:1: ( ( ( rule__NotMappedCause__NameAssignment_2 ) ) )
            // InternalDOF.g:1530:1: ( ( rule__NotMappedCause__NameAssignment_2 ) )
            {
            // InternalDOF.g:1530:1: ( ( rule__NotMappedCause__NameAssignment_2 ) )
            // InternalDOF.g:1531:2: ( rule__NotMappedCause__NameAssignment_2 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getNameAssignment_2()); 
            // InternalDOF.g:1532:2: ( rule__NotMappedCause__NameAssignment_2 )
            // InternalDOF.g:1532:3: rule__NotMappedCause__NameAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__NameAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getNameAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__2__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__3"
    // InternalDOF.g:1540:1: rule__NotMappedCause__Group__3 : rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4 ;
    public final void rule__NotMappedCause__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1544:1: ( rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4 )
            // InternalDOF.g:1545:2: rule__NotMappedCause__Group__3__Impl rule__NotMappedCause__Group__4
            {
            pushFollow(FOLLOW_17);
            rule__NotMappedCause__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__3"


    // $ANTLR start "rule__NotMappedCause__Group__3__Impl"
    // InternalDOF.g:1552:1: rule__NotMappedCause__Group__3__Impl : ( ( rule__NotMappedCause__Group_3__0 )? ) ;
    public final void rule__NotMappedCause__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1556:1: ( ( ( rule__NotMappedCause__Group_3__0 )? ) )
            // InternalDOF.g:1557:1: ( ( rule__NotMappedCause__Group_3__0 )? )
            {
            // InternalDOF.g:1557:1: ( ( rule__NotMappedCause__Group_3__0 )? )
            // InternalDOF.g:1558:2: ( rule__NotMappedCause__Group_3__0 )?
            {
             before(grammarAccess.getNotMappedCauseAccess().getGroup_3()); 
            // InternalDOF.g:1559:2: ( rule__NotMappedCause__Group_3__0 )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==22) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDOF.g:1559:3: rule__NotMappedCause__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__NotMappedCause__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getNotMappedCauseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__3__Impl"


    // $ANTLR start "rule__NotMappedCause__Group__4"
    // InternalDOF.g:1567:1: rule__NotMappedCause__Group__4 : rule__NotMappedCause__Group__4__Impl ;
    public final void rule__NotMappedCause__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1571:1: ( rule__NotMappedCause__Group__4__Impl )
            // InternalDOF.g:1572:2: rule__NotMappedCause__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__4"


    // $ANTLR start "rule__NotMappedCause__Group__4__Impl"
    // InternalDOF.g:1578:1: rule__NotMappedCause__Group__4__Impl : ( 'notMapped' ) ;
    public final void rule__NotMappedCause__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1582:1: ( ( 'notMapped' ) )
            // InternalDOF.g:1583:1: ( 'notMapped' )
            {
            // InternalDOF.g:1583:1: ( 'notMapped' )
            // InternalDOF.g:1584:2: 'notMapped'
            {
             before(grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group__4__Impl"


    // $ANTLR start "rule__NotMappedCause__Group_3__0"
    // InternalDOF.g:1594:1: rule__NotMappedCause__Group_3__0 : rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1 ;
    public final void rule__NotMappedCause__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1598:1: ( rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1 )
            // InternalDOF.g:1599:2: rule__NotMappedCause__Group_3__0__Impl rule__NotMappedCause__Group_3__1
            {
            pushFollow(FOLLOW_15);
            rule__NotMappedCause__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__0"


    // $ANTLR start "rule__NotMappedCause__Group_3__0__Impl"
    // InternalDOF.g:1606:1: rule__NotMappedCause__Group_3__0__Impl : ( 'realizes' ) ;
    public final void rule__NotMappedCause__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1610:1: ( ( 'realizes' ) )
            // InternalDOF.g:1611:1: ( 'realizes' )
            {
            // InternalDOF.g:1611:1: ( 'realizes' )
            // InternalDOF.g:1612:2: 'realizes'
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__0__Impl"


    // $ANTLR start "rule__NotMappedCause__Group_3__1"
    // InternalDOF.g:1621:1: rule__NotMappedCause__Group_3__1 : rule__NotMappedCause__Group_3__1__Impl ;
    public final void rule__NotMappedCause__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1625:1: ( rule__NotMappedCause__Group_3__1__Impl )
            // InternalDOF.g:1626:2: rule__NotMappedCause__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__1"


    // $ANTLR start "rule__NotMappedCause__Group_3__1__Impl"
    // InternalDOF.g:1632:1: rule__NotMappedCause__Group_3__1__Impl : ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) ) ;
    public final void rule__NotMappedCause__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1636:1: ( ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) ) )
            // InternalDOF.g:1637:1: ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) )
            {
            // InternalDOF.g:1637:1: ( ( rule__NotMappedCause__RealizesAssignment_3_1 ) )
            // InternalDOF.g:1638:2: ( rule__NotMappedCause__RealizesAssignment_3_1 )
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesAssignment_3_1()); 
            // InternalDOF.g:1639:2: ( rule__NotMappedCause__RealizesAssignment_3_1 )
            // InternalDOF.g:1639:3: rule__NotMappedCause__RealizesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__NotMappedCause__RealizesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getNotMappedCauseAccess().getRealizesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__Group_3__1__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0"
    // InternalDOF.g:1648:1: rule__QualifiedNameWithWildcard__Group__0 : rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 ;
    public final void rule__QualifiedNameWithWildcard__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1652:1: ( rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1 )
            // InternalDOF.g:1653:2: rule__QualifiedNameWithWildcard__Group__0__Impl rule__QualifiedNameWithWildcard__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__QualifiedNameWithWildcard__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__0__Impl"
    // InternalDOF.g:1660:1: rule__QualifiedNameWithWildcard__Group__0__Impl : ( ruleQualifiedName ) ;
    public final void rule__QualifiedNameWithWildcard__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1664:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:1665:1: ( ruleQualifiedName )
            {
            // InternalDOF.g:1665:1: ( ruleQualifiedName )
            // InternalDOF.g:1666:2: ruleQualifiedName
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__0__Impl"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1"
    // InternalDOF.g:1675:1: rule__QualifiedNameWithWildcard__Group__1 : rule__QualifiedNameWithWildcard__Group__1__Impl ;
    public final void rule__QualifiedNameWithWildcard__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1679:1: ( rule__QualifiedNameWithWildcard__Group__1__Impl )
            // InternalDOF.g:1680:2: rule__QualifiedNameWithWildcard__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedNameWithWildcard__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1"


    // $ANTLR start "rule__QualifiedNameWithWildcard__Group__1__Impl"
    // InternalDOF.g:1686:1: rule__QualifiedNameWithWildcard__Group__1__Impl : ( ( '.*' )? ) ;
    public final void rule__QualifiedNameWithWildcard__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1690:1: ( ( ( '.*' )? ) )
            // InternalDOF.g:1691:1: ( ( '.*' )? )
            {
            // InternalDOF.g:1691:1: ( ( '.*' )? )
            // InternalDOF.g:1692:2: ( '.*' )?
            {
             before(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 
            // InternalDOF.g:1693:2: ( '.*' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==24) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDOF.g:1693:3: '.*'
                    {
                    match(input,24,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedNameWithWildcard__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group__0"
    // InternalDOF.g:1702:1: rule__QualifiedName__Group__0 : rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 ;
    public final void rule__QualifiedName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1706:1: ( rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1 )
            // InternalDOF.g:1707:2: rule__QualifiedName__Group__0__Impl rule__QualifiedName__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__QualifiedName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0"


    // $ANTLR start "rule__QualifiedName__Group__0__Impl"
    // InternalDOF.g:1714:1: rule__QualifiedName__Group__0__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1718:1: ( ( RULE_ID ) )
            // InternalDOF.g:1719:1: ( RULE_ID )
            {
            // InternalDOF.g:1719:1: ( RULE_ID )
            // InternalDOF.g:1720:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group__1"
    // InternalDOF.g:1729:1: rule__QualifiedName__Group__1 : rule__QualifiedName__Group__1__Impl ;
    public final void rule__QualifiedName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1733:1: ( rule__QualifiedName__Group__1__Impl )
            // InternalDOF.g:1734:2: rule__QualifiedName__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1"


    // $ANTLR start "rule__QualifiedName__Group__1__Impl"
    // InternalDOF.g:1740:1: rule__QualifiedName__Group__1__Impl : ( ( rule__QualifiedName__Group_1__0 )* ) ;
    public final void rule__QualifiedName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1744:1: ( ( ( rule__QualifiedName__Group_1__0 )* ) )
            // InternalDOF.g:1745:1: ( ( rule__QualifiedName__Group_1__0 )* )
            {
            // InternalDOF.g:1745:1: ( ( rule__QualifiedName__Group_1__0 )* )
            // InternalDOF.g:1746:2: ( rule__QualifiedName__Group_1__0 )*
            {
             before(grammarAccess.getQualifiedNameAccess().getGroup_1()); 
            // InternalDOF.g:1747:2: ( rule__QualifiedName__Group_1__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==25) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalDOF.g:1747:3: rule__QualifiedName__Group_1__0
            	    {
            	    pushFollow(FOLLOW_20);
            	    rule__QualifiedName__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getQualifiedNameAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group__1__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__0"
    // InternalDOF.g:1756:1: rule__QualifiedName__Group_1__0 : rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 ;
    public final void rule__QualifiedName__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1760:1: ( rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1 )
            // InternalDOF.g:1761:2: rule__QualifiedName__Group_1__0__Impl rule__QualifiedName__Group_1__1
            {
            pushFollow(FOLLOW_5);
            rule__QualifiedName__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0"


    // $ANTLR start "rule__QualifiedName__Group_1__0__Impl"
    // InternalDOF.g:1768:1: rule__QualifiedName__Group_1__0__Impl : ( '.' ) ;
    public final void rule__QualifiedName__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1772:1: ( ( '.' ) )
            // InternalDOF.g:1773:1: ( '.' )
            {
            // InternalDOF.g:1773:1: ( '.' )
            // InternalDOF.g:1774:2: '.'
            {
             before(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__0__Impl"


    // $ANTLR start "rule__QualifiedName__Group_1__1"
    // InternalDOF.g:1783:1: rule__QualifiedName__Group_1__1 : rule__QualifiedName__Group_1__1__Impl ;
    public final void rule__QualifiedName__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1787:1: ( rule__QualifiedName__Group_1__1__Impl )
            // InternalDOF.g:1788:2: rule__QualifiedName__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QualifiedName__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1"


    // $ANTLR start "rule__QualifiedName__Group_1__1__Impl"
    // InternalDOF.g:1794:1: rule__QualifiedName__Group_1__1__Impl : ( RULE_ID ) ;
    public final void rule__QualifiedName__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1798:1: ( ( RULE_ID ) )
            // InternalDOF.g:1799:1: ( RULE_ID )
            {
            // InternalDOF.g:1799:1: ( RULE_ID )
            // InternalDOF.g:1800:2: RULE_ID
            {
             before(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QualifiedName__Group_1__1__Impl"


    // $ANTLR start "rule__DOF__NameAssignment_0_1"
    // InternalDOF.g:1810:1: rule__DOF__NameAssignment_0_1 : ( ruleQualifiedName ) ;
    public final void rule__DOF__NameAssignment_0_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1814:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:1815:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:1815:2: ( ruleQualifiedName )
            // InternalDOF.g:1816:3: ruleQualifiedName
            {
             before(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_0_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__NameAssignment_0_1"


    // $ANTLR start "rule__DOF__ImportsAssignment_1"
    // InternalDOF.g:1825:1: rule__DOF__ImportsAssignment_1 : ( ruleImport ) ;
    public final void rule__DOF__ImportsAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1829:1: ( ( ruleImport ) )
            // InternalDOF.g:1830:2: ( ruleImport )
            {
            // InternalDOF.g:1830:2: ( ruleImport )
            // InternalDOF.g:1831:3: ruleImport
            {
             before(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleImport();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__ImportsAssignment_1"


    // $ANTLR start "rule__DOF__EffectsAssignment_2"
    // InternalDOF.g:1840:1: rule__DOF__EffectsAssignment_2 : ( ruleEffect ) ;
    public final void rule__DOF__EffectsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1844:1: ( ( ruleEffect ) )
            // InternalDOF.g:1845:2: ( ruleEffect )
            {
            // InternalDOF.g:1845:2: ( ruleEffect )
            // InternalDOF.g:1846:3: ruleEffect
            {
             before(grammarAccess.getDOFAccess().getEffectsEffectParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEffect();

            state._fsp--;

             after(grammarAccess.getDOFAccess().getEffectsEffectParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DOF__EffectsAssignment_2"


    // $ANTLR start "rule__Import__ImportedNamespaceAssignment_1"
    // InternalDOF.g:1855:1: rule__Import__ImportedNamespaceAssignment_1 : ( ruleQualifiedNameWithWildcard ) ;
    public final void rule__Import__ImportedNamespaceAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1859:1: ( ( ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:1860:2: ( ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:1860:2: ( ruleQualifiedNameWithWildcard )
            // InternalDOF.g:1861:3: ruleQualifiedNameWithWildcard
            {
             before(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedNameWithWildcard();

            state._fsp--;

             after(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Import__ImportedNamespaceAssignment_1"


    // $ANTLR start "rule__Effect__NameAssignment_1"
    // InternalDOF.g:1870:1: rule__Effect__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Effect__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1874:1: ( ( RULE_ID ) )
            // InternalDOF.g:1875:2: ( RULE_ID )
            {
            // InternalDOF.g:1875:2: ( RULE_ID )
            // InternalDOF.g:1876:3: RULE_ID
            {
             before(grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__NameAssignment_1"


    // $ANTLR start "rule__Effect__DataFeederAssignment_3"
    // InternalDOF.g:1885:1: rule__Effect__DataFeederAssignment_3 : ( ruleDataFeeder ) ;
    public final void rule__Effect__DataFeederAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1889:1: ( ( ruleDataFeeder ) )
            // InternalDOF.g:1890:2: ( ruleDataFeeder )
            {
            // InternalDOF.g:1890:2: ( ruleDataFeeder )
            // InternalDOF.g:1891:3: ruleDataFeeder
            {
             before(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleDataFeeder();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__DataFeederAssignment_3"


    // $ANTLR start "rule__Effect__CategoriesAssignment_5"
    // InternalDOF.g:1900:1: rule__Effect__CategoriesAssignment_5 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1904:1: ( ( ruleCategory ) )
            // InternalDOF.g:1905:2: ( ruleCategory )
            {
            // InternalDOF.g:1905:2: ( ruleCategory )
            // InternalDOF.g:1906:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_5"


    // $ANTLR start "rule__Effect__CategoriesAssignment_6"
    // InternalDOF.g:1915:1: rule__Effect__CategoriesAssignment_6 : ( ruleCategory ) ;
    public final void rule__Effect__CategoriesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1919:1: ( ( ruleCategory ) )
            // InternalDOF.g:1920:2: ( ruleCategory )
            {
            // InternalDOF.g:1920:2: ( ruleCategory )
            // InternalDOF.g:1921:3: ruleCategory
            {
             before(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCategory();

            state._fsp--;

             after(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Effect__CategoriesAssignment_6"


    // $ANTLR start "rule__Category__NameAssignment_1"
    // InternalDOF.g:1930:1: rule__Category__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Category__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1934:1: ( ( RULE_ID ) )
            // InternalDOF.g:1935:2: ( RULE_ID )
            {
            // InternalDOF.g:1935:2: ( RULE_ID )
            // InternalDOF.g:1936:3: RULE_ID
            {
             before(grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__NameAssignment_1"


    // $ANTLR start "rule__Category__TypeAssignment_2_1"
    // InternalDOF.g:1945:1: rule__Category__TypeAssignment_2_1 : ( ( ruleQualifiedName ) ) ;
    public final void rule__Category__TypeAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1949:1: ( ( ( ruleQualifiedName ) ) )
            // InternalDOF.g:1950:2: ( ( ruleQualifiedName ) )
            {
            // InternalDOF.g:1950:2: ( ( ruleQualifiedName ) )
            // InternalDOF.g:1951:3: ( ruleQualifiedName )
            {
             before(grammarAccess.getCategoryAccess().getTypeCauseCrossReference_2_1_0()); 
            // InternalDOF.g:1952:3: ( ruleQualifiedName )
            // InternalDOF.g:1953:4: ruleQualifiedName
            {
             before(grammarAccess.getCategoryAccess().getTypeCauseQualifiedNameParserRuleCall_2_1_0_1()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getTypeCauseQualifiedNameParserRuleCall_2_1_0_1()); 

            }

             after(grammarAccess.getCategoryAccess().getTypeCauseCrossReference_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__TypeAssignment_2_1"


    // $ANTLR start "rule__Category__CausesAssignment_3"
    // InternalDOF.g:1964:1: rule__Category__CausesAssignment_3 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1968:1: ( ( ruleCause ) )
            // InternalDOF.g:1969:2: ( ruleCause )
            {
            // InternalDOF.g:1969:2: ( ruleCause )
            // InternalDOF.g:1970:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_3"


    // $ANTLR start "rule__Category__CausesAssignment_4"
    // InternalDOF.g:1979:1: rule__Category__CausesAssignment_4 : ( ruleCause ) ;
    public final void rule__Category__CausesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1983:1: ( ( ruleCause ) )
            // InternalDOF.g:1984:2: ( ruleCause )
            {
            // InternalDOF.g:1984:2: ( ruleCause )
            // InternalDOF.g:1985:3: ruleCause
            {
             before(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Category__CausesAssignment_4"


    // $ANTLR start "rule__DataFeeder__NameAssignment_1"
    // InternalDOF.g:1994:1: rule__DataFeeder__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__DataFeeder__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:1998:1: ( ( RULE_ID ) )
            // InternalDOF.g:1999:2: ( RULE_ID )
            {
            // InternalDOF.g:1999:2: ( RULE_ID )
            // InternalDOF.g:2000:3: RULE_ID
            {
             before(grammarAccess.getDataFeederAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDataFeederAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataFeeder__NameAssignment_1"


    // $ANTLR start "rule__CompoundCause__NameAssignment_1"
    // InternalDOF.g:2009:1: rule__CompoundCause__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__CompoundCause__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2013:1: ( ( RULE_ID ) )
            // InternalDOF.g:2014:2: ( RULE_ID )
            {
            // InternalDOF.g:2014:2: ( RULE_ID )
            // InternalDOF.g:2015:3: RULE_ID
            {
             before(grammarAccess.getCompoundCauseAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getCompoundCauseAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__NameAssignment_1"


    // $ANTLR start "rule__CompoundCause__RealizesAssignment_2_1"
    // InternalDOF.g:2024:1: rule__CompoundCause__RealizesAssignment_2_1 : ( ruleEString ) ;
    public final void rule__CompoundCause__RealizesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2028:1: ( ( ruleEString ) )
            // InternalDOF.g:2029:2: ( ruleEString )
            {
            // InternalDOF.g:2029:2: ( ruleEString )
            // InternalDOF.g:2030:3: ruleEString
            {
             before(grammarAccess.getCompoundCauseAccess().getRealizesEStringParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getRealizesEStringParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__RealizesAssignment_2_1"


    // $ANTLR start "rule__CompoundCause__SubCausesAssignment_5"
    // InternalDOF.g:2039:1: rule__CompoundCause__SubCausesAssignment_5 : ( ruleCause ) ;
    public final void rule__CompoundCause__SubCausesAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2043:1: ( ( ruleCause ) )
            // InternalDOF.g:2044:2: ( ruleCause )
            {
            // InternalDOF.g:2044:2: ( ruleCause )
            // InternalDOF.g:2045:3: ruleCause
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__SubCausesAssignment_5"


    // $ANTLR start "rule__CompoundCause__SubCausesAssignment_6"
    // InternalDOF.g:2054:1: rule__CompoundCause__SubCausesAssignment_6 : ( ruleCause ) ;
    public final void rule__CompoundCause__SubCausesAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2058:1: ( ( ruleCause ) )
            // InternalDOF.g:2059:2: ( ruleCause )
            {
            // InternalDOF.g:2059:2: ( ruleCause )
            // InternalDOF.g:2060:3: ruleCause
            {
             before(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleCause();

            state._fsp--;

             after(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CompoundCause__SubCausesAssignment_6"


    // $ANTLR start "rule__DataLinkedCause__NameAssignment_1"
    // InternalDOF.g:2069:1: rule__DataLinkedCause__NameAssignment_1 : ( ruleQualifiedName ) ;
    public final void rule__DataLinkedCause__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2073:1: ( ( ruleQualifiedName ) )
            // InternalDOF.g:2074:2: ( ruleQualifiedName )
            {
            // InternalDOF.g:2074:2: ( ruleQualifiedName )
            // InternalDOF.g:2075:3: ruleQualifiedName
            {
             before(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleQualifiedName();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__NameAssignment_1"


    // $ANTLR start "rule__DataLinkedCause__RealizesAssignment_2_1"
    // InternalDOF.g:2084:1: rule__DataLinkedCause__RealizesAssignment_2_1 : ( ruleEString ) ;
    public final void rule__DataLinkedCause__RealizesAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2088:1: ( ( ruleEString ) )
            // InternalDOF.g:2089:2: ( ruleEString )
            {
            // InternalDOF.g:2089:2: ( ruleEString )
            // InternalDOF.g:2090:3: ruleEString
            {
             before(grammarAccess.getDataLinkedCauseAccess().getRealizesEStringParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getRealizesEStringParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__RealizesAssignment_2_1"


    // $ANTLR start "rule__DataLinkedCause__DataFeederAssignment_4"
    // InternalDOF.g:2099:1: rule__DataLinkedCause__DataFeederAssignment_4 : ( ruleDataFeeder ) ;
    public final void rule__DataLinkedCause__DataFeederAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2103:1: ( ( ruleDataFeeder ) )
            // InternalDOF.g:2104:2: ( ruleDataFeeder )
            {
            // InternalDOF.g:2104:2: ( ruleDataFeeder )
            // InternalDOF.g:2105:3: ruleDataFeeder
            {
             before(grammarAccess.getDataLinkedCauseAccess().getDataFeederDataFeederParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleDataFeeder();

            state._fsp--;

             after(grammarAccess.getDataLinkedCauseAccess().getDataFeederDataFeederParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DataLinkedCause__DataFeederAssignment_4"


    // $ANTLR start "rule__NotMappedCause__NameAssignment_2"
    // InternalDOF.g:2114:1: rule__NotMappedCause__NameAssignment_2 : ( RULE_ID ) ;
    public final void rule__NotMappedCause__NameAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2118:1: ( ( RULE_ID ) )
            // InternalDOF.g:2119:2: ( RULE_ID )
            {
            // InternalDOF.g:2119:2: ( RULE_ID )
            // InternalDOF.g:2120:3: RULE_ID
            {
             before(grammarAccess.getNotMappedCauseAccess().getNameIDTerminalRuleCall_2_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getNotMappedCauseAccess().getNameIDTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__NameAssignment_2"


    // $ANTLR start "rule__NotMappedCause__RealizesAssignment_3_1"
    // InternalDOF.g:2129:1: rule__NotMappedCause__RealizesAssignment_3_1 : ( ruleEString ) ;
    public final void rule__NotMappedCause__RealizesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalDOF.g:2133:1: ( ( ruleEString ) )
            // InternalDOF.g:2134:2: ( ruleEString )
            {
            // InternalDOF.g:2134:2: ( ruleEString )
            // InternalDOF.g:2135:3: ruleEString
            {
             before(grammarAccess.getNotMappedCauseAccess().getRealizesEStringParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getNotMappedCauseAccess().getRealizesEStringParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__NotMappedCause__RealizesAssignment_3_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000060002L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000480000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000260000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000404000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000C00000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000002L});

}