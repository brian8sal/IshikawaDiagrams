/**
 * generated by Xtext 2.24.0
 */
package dataOrientedFishbone.dof;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Not Mapped Cause</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see dataOrientedFishbone.dof.DofPackage#getNotMappedCause()
 * @model
 * @generated
 */
public interface NotMappedCause extends Cause
{
} // NotMappedCause
