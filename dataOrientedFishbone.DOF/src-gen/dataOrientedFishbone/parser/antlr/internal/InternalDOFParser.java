package dataOrientedFishbone.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import dataOrientedFishbone.services.DOFGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalDOFParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'dof'", "'import'", "'effect'", "'is'", "'include'", "'category'", "'otras'", "'cause'", "'realizes'", "'contains'", "'{'", "'}'", "'notMapped'", "'.*'", "'.'"
    };
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalDOFParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalDOFParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalDOFParser.tokenNames; }
    public String getGrammarFileName() { return "InternalDOF.g"; }



     	private DOFGrammarAccess grammarAccess;

        public InternalDOFParser(TokenStream input, DOFGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "DOF";
       	}

       	@Override
       	protected DOFGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleDOF"
    // InternalDOF.g:64:1: entryRuleDOF returns [EObject current=null] : iv_ruleDOF= ruleDOF EOF ;
    public final EObject entryRuleDOF() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDOF = null;


        try {
            // InternalDOF.g:64:44: (iv_ruleDOF= ruleDOF EOF )
            // InternalDOF.g:65:2: iv_ruleDOF= ruleDOF EOF
            {
             newCompositeNode(grammarAccess.getDOFRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDOF=ruleDOF();

            state._fsp--;

             current =iv_ruleDOF; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDOF"


    // $ANTLR start "ruleDOF"
    // InternalDOF.g:71:1: ruleDOF returns [EObject current=null] : ( (otherlv_0= 'dof' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_imports_2_0= ruleImport ) )* ( (lv_effects_3_0= ruleEffect ) ) ) ;
    public final EObject ruleDOF() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_imports_2_0 = null;

        EObject lv_effects_3_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:77:2: ( ( (otherlv_0= 'dof' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_imports_2_0= ruleImport ) )* ( (lv_effects_3_0= ruleEffect ) ) ) )
            // InternalDOF.g:78:2: ( (otherlv_0= 'dof' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_imports_2_0= ruleImport ) )* ( (lv_effects_3_0= ruleEffect ) ) )
            {
            // InternalDOF.g:78:2: ( (otherlv_0= 'dof' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_imports_2_0= ruleImport ) )* ( (lv_effects_3_0= ruleEffect ) ) )
            // InternalDOF.g:79:3: (otherlv_0= 'dof' ( (lv_name_1_0= ruleQualifiedName ) ) )? ( (lv_imports_2_0= ruleImport ) )* ( (lv_effects_3_0= ruleEffect ) )
            {
            // InternalDOF.g:79:3: (otherlv_0= 'dof' ( (lv_name_1_0= ruleQualifiedName ) ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalDOF.g:80:4: otherlv_0= 'dof' ( (lv_name_1_0= ruleQualifiedName ) )
                    {
                    otherlv_0=(Token)match(input,11,FOLLOW_3); 

                    				newLeafNode(otherlv_0, grammarAccess.getDOFAccess().getDofKeyword_0_0());
                    			
                    // InternalDOF.g:84:4: ( (lv_name_1_0= ruleQualifiedName ) )
                    // InternalDOF.g:85:5: (lv_name_1_0= ruleQualifiedName )
                    {
                    // InternalDOF.g:85:5: (lv_name_1_0= ruleQualifiedName )
                    // InternalDOF.g:86:6: lv_name_1_0= ruleQualifiedName
                    {

                    						newCompositeNode(grammarAccess.getDOFAccess().getNameQualifiedNameParserRuleCall_0_1_0());
                    					
                    pushFollow(FOLLOW_4);
                    lv_name_1_0=ruleQualifiedName();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDOFRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_1_0,
                    							"dataOrientedFishbone.DOF.QualifiedName");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDOF.g:104:3: ( (lv_imports_2_0= ruleImport ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalDOF.g:105:4: (lv_imports_2_0= ruleImport )
            	    {
            	    // InternalDOF.g:105:4: (lv_imports_2_0= ruleImport )
            	    // InternalDOF.g:106:5: lv_imports_2_0= ruleImport
            	    {

            	    					newCompositeNode(grammarAccess.getDOFAccess().getImportsImportParserRuleCall_1_0());
            	    				
            	    pushFollow(FOLLOW_4);
            	    lv_imports_2_0=ruleImport();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getDOFRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_2_0,
            	    						"dataOrientedFishbone.DOF.Import");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // InternalDOF.g:123:3: ( (lv_effects_3_0= ruleEffect ) )
            // InternalDOF.g:124:4: (lv_effects_3_0= ruleEffect )
            {
            // InternalDOF.g:124:4: (lv_effects_3_0= ruleEffect )
            // InternalDOF.g:125:5: lv_effects_3_0= ruleEffect
            {

            					newCompositeNode(grammarAccess.getDOFAccess().getEffectsEffectParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_effects_3_0=ruleEffect();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDOFRule());
            					}
            					add(
            						current,
            						"effects",
            						lv_effects_3_0,
            						"dataOrientedFishbone.DOF.Effect");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDOF"


    // $ANTLR start "entryRuleImport"
    // InternalDOF.g:146:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalDOF.g:146:47: (iv_ruleImport= ruleImport EOF )
            // InternalDOF.g:147:2: iv_ruleImport= ruleImport EOF
            {
             newCompositeNode(grammarAccess.getImportRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;

             current =iv_ruleImport; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalDOF.g:153:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:159:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) ) )
            // InternalDOF.g:160:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            {
            // InternalDOF.g:160:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) )
            // InternalDOF.g:161:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
            		
            // InternalDOF.g:165:3: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalDOF.g:166:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalDOF.g:166:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // InternalDOF.g:167:5: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {

            					newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getImportRule());
            					}
            					set(
            						current,
            						"importedNamespace",
            						lv_importedNamespace_1_0,
            						"dataOrientedFishbone.DOF.QualifiedNameWithWildcard");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleEffect"
    // InternalDOF.g:188:1: entryRuleEffect returns [EObject current=null] : iv_ruleEffect= ruleEffect EOF ;
    public final EObject entryRuleEffect() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleEffect = null;


        try {
            // InternalDOF.g:188:47: (iv_ruleEffect= ruleEffect EOF )
            // InternalDOF.g:189:2: iv_ruleEffect= ruleEffect EOF
            {
             newCompositeNode(grammarAccess.getEffectRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEffect=ruleEffect();

            state._fsp--;

             current =iv_ruleEffect; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEffect"


    // $ANTLR start "ruleEffect"
    // InternalDOF.g:195:1: ruleEffect returns [EObject current=null] : (otherlv_0= 'effect' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' ( (lv_dataFeeder_3_0= ruleDataFeeder ) ) otherlv_4= 'include' ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* ) ;
    public final EObject ruleEffect() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_dataFeeder_3_0 = null;

        EObject lv_categories_5_0 = null;

        EObject lv_categories_6_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:201:2: ( (otherlv_0= 'effect' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' ( (lv_dataFeeder_3_0= ruleDataFeeder ) ) otherlv_4= 'include' ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* ) )
            // InternalDOF.g:202:2: (otherlv_0= 'effect' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' ( (lv_dataFeeder_3_0= ruleDataFeeder ) ) otherlv_4= 'include' ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )
            {
            // InternalDOF.g:202:2: (otherlv_0= 'effect' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' ( (lv_dataFeeder_3_0= ruleDataFeeder ) ) otherlv_4= 'include' ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )* )
            // InternalDOF.g:203:3: otherlv_0= 'effect' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= 'is' ( (lv_dataFeeder_3_0= ruleDataFeeder ) ) otherlv_4= 'include' ( (lv_categories_5_0= ruleCategory ) ) ( (lv_categories_6_0= ruleCategory ) )*
            {
            otherlv_0=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getEffectAccess().getEffectKeyword_0());
            		
            // InternalDOF.g:207:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDOF.g:208:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDOF.g:208:4: (lv_name_1_0= RULE_ID )
            // InternalDOF.g:209:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_5); 

            					newLeafNode(lv_name_1_0, grammarAccess.getEffectAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getEffectRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_2, grammarAccess.getEffectAccess().getIsKeyword_2());
            		
            // InternalDOF.g:229:3: ( (lv_dataFeeder_3_0= ruleDataFeeder ) )
            // InternalDOF.g:230:4: (lv_dataFeeder_3_0= ruleDataFeeder )
            {
            // InternalDOF.g:230:4: (lv_dataFeeder_3_0= ruleDataFeeder )
            // InternalDOF.g:231:5: lv_dataFeeder_3_0= ruleDataFeeder
            {

            					newCompositeNode(grammarAccess.getEffectAccess().getDataFeederDataFeederParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_6);
            lv_dataFeeder_3_0=ruleDataFeeder();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectRule());
            					}
            					set(
            						current,
            						"dataFeeder",
            						lv_dataFeeder_3_0,
            						"dataOrientedFishbone.DOF.DataFeeder");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_7); 

            			newLeafNode(otherlv_4, grammarAccess.getEffectAccess().getIncludeKeyword_4());
            		
            // InternalDOF.g:252:3: ( (lv_categories_5_0= ruleCategory ) )
            // InternalDOF.g:253:4: (lv_categories_5_0= ruleCategory )
            {
            // InternalDOF.g:253:4: (lv_categories_5_0= ruleCategory )
            // InternalDOF.g:254:5: lv_categories_5_0= ruleCategory
            {

            					newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_8);
            lv_categories_5_0=ruleCategory();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getEffectRule());
            					}
            					add(
            						current,
            						"categories",
            						lv_categories_5_0,
            						"dataOrientedFishbone.DOF.Category");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:271:3: ( (lv_categories_6_0= ruleCategory ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==16) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalDOF.g:272:4: (lv_categories_6_0= ruleCategory )
            	    {
            	    // InternalDOF.g:272:4: (lv_categories_6_0= ruleCategory )
            	    // InternalDOF.g:273:5: lv_categories_6_0= ruleCategory
            	    {

            	    					newCompositeNode(grammarAccess.getEffectAccess().getCategoriesCategoryParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_8);
            	    lv_categories_6_0=ruleCategory();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getEffectRule());
            	    					}
            	    					add(
            	    						current,
            	    						"categories",
            	    						lv_categories_6_0,
            	    						"dataOrientedFishbone.DOF.Category");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEffect"


    // $ANTLR start "entryRuleCategory"
    // InternalDOF.g:294:1: entryRuleCategory returns [EObject current=null] : iv_ruleCategory= ruleCategory EOF ;
    public final EObject entryRuleCategory() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCategory = null;


        try {
            // InternalDOF.g:294:49: (iv_ruleCategory= ruleCategory EOF )
            // InternalDOF.g:295:2: iv_ruleCategory= ruleCategory EOF
            {
             newCompositeNode(grammarAccess.getCategoryRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCategory=ruleCategory();

            state._fsp--;

             current =iv_ruleCategory; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCategory"


    // $ANTLR start "ruleCategory"
    // InternalDOF.g:301:1: ruleCategory returns [EObject current=null] : (otherlv_0= 'category' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'otras' ( ( ruleQualifiedName ) ) )? ( (lv_causes_4_0= ruleCause ) ) ( (lv_causes_5_0= ruleCause ) )* ) ;
    public final EObject ruleCategory() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        EObject lv_causes_4_0 = null;

        EObject lv_causes_5_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:307:2: ( (otherlv_0= 'category' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'otras' ( ( ruleQualifiedName ) ) )? ( (lv_causes_4_0= ruleCause ) ) ( (lv_causes_5_0= ruleCause ) )* ) )
            // InternalDOF.g:308:2: (otherlv_0= 'category' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'otras' ( ( ruleQualifiedName ) ) )? ( (lv_causes_4_0= ruleCause ) ) ( (lv_causes_5_0= ruleCause ) )* )
            {
            // InternalDOF.g:308:2: (otherlv_0= 'category' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'otras' ( ( ruleQualifiedName ) ) )? ( (lv_causes_4_0= ruleCause ) ) ( (lv_causes_5_0= ruleCause ) )* )
            // InternalDOF.g:309:3: otherlv_0= 'category' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'otras' ( ( ruleQualifiedName ) ) )? ( (lv_causes_4_0= ruleCause ) ) ( (lv_causes_5_0= ruleCause ) )*
            {
            otherlv_0=(Token)match(input,16,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getCategoryAccess().getCategoryKeyword_0());
            		
            // InternalDOF.g:313:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDOF.g:314:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDOF.g:314:4: (lv_name_1_0= RULE_ID )
            // InternalDOF.g:315:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_9); 

            					newLeafNode(lv_name_1_0, grammarAccess.getCategoryAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCategoryRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:331:3: (otherlv_2= 'otras' ( ( ruleQualifiedName ) ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==17) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalDOF.g:332:4: otherlv_2= 'otras' ( ( ruleQualifiedName ) )
                    {
                    otherlv_2=(Token)match(input,17,FOLLOW_3); 

                    				newLeafNode(otherlv_2, grammarAccess.getCategoryAccess().getOtrasKeyword_2_0());
                    			
                    // InternalDOF.g:336:4: ( ( ruleQualifiedName ) )
                    // InternalDOF.g:337:5: ( ruleQualifiedName )
                    {
                    // InternalDOF.g:337:5: ( ruleQualifiedName )
                    // InternalDOF.g:338:6: ruleQualifiedName
                    {

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getCategoryRule());
                    						}
                    					

                    						newCompositeNode(grammarAccess.getCategoryAccess().getTypeCauseCrossReference_2_1_0());
                    					
                    pushFollow(FOLLOW_9);
                    ruleQualifiedName();

                    state._fsp--;


                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            // InternalDOF.g:353:3: ( (lv_causes_4_0= ruleCause ) )
            // InternalDOF.g:354:4: (lv_causes_4_0= ruleCause )
            {
            // InternalDOF.g:354:4: (lv_causes_4_0= ruleCause )
            // InternalDOF.g:355:5: lv_causes_4_0= ruleCause
            {

            					newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_10);
            lv_causes_4_0=ruleCause();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCategoryRule());
            					}
            					add(
            						current,
            						"causes",
            						lv_causes_4_0,
            						"dataOrientedFishbone.DOF.Cause");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:372:3: ( (lv_causes_5_0= ruleCause ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==18) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalDOF.g:373:4: (lv_causes_5_0= ruleCause )
            	    {
            	    // InternalDOF.g:373:4: (lv_causes_5_0= ruleCause )
            	    // InternalDOF.g:374:5: lv_causes_5_0= ruleCause
            	    {

            	    					newCompositeNode(grammarAccess.getCategoryAccess().getCausesCauseParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_10);
            	    lv_causes_5_0=ruleCause();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCategoryRule());
            	    					}
            	    					add(
            	    						current,
            	    						"causes",
            	    						lv_causes_5_0,
            	    						"dataOrientedFishbone.DOF.Cause");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCategory"


    // $ANTLR start "entryRuleDataFeeder"
    // InternalDOF.g:395:1: entryRuleDataFeeder returns [EObject current=null] : iv_ruleDataFeeder= ruleDataFeeder EOF ;
    public final EObject entryRuleDataFeeder() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataFeeder = null;


        try {
            // InternalDOF.g:395:51: (iv_ruleDataFeeder= ruleDataFeeder EOF )
            // InternalDOF.g:396:2: iv_ruleDataFeeder= ruleDataFeeder EOF
            {
             newCompositeNode(grammarAccess.getDataFeederRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataFeeder=ruleDataFeeder();

            state._fsp--;

             current =iv_ruleDataFeeder; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataFeeder"


    // $ANTLR start "ruleDataFeeder"
    // InternalDOF.g:402:1: ruleDataFeeder returns [EObject current=null] : ( () ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleDataFeeder() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalDOF.g:408:2: ( ( () ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalDOF.g:409:2: ( () ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalDOF.g:409:2: ( () ( (lv_name_1_0= RULE_ID ) ) )
            // InternalDOF.g:410:3: () ( (lv_name_1_0= RULE_ID ) )
            {
            // InternalDOF.g:410:3: ()
            // InternalDOF.g:411:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDataFeederAccess().getDataFeederAction_0(),
            					current);
            			

            }

            // InternalDOF.g:417:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDOF.g:418:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDOF.g:418:4: (lv_name_1_0= RULE_ID )
            // InternalDOF.g:419:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getDataFeederAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDataFeederRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataFeeder"


    // $ANTLR start "entryRuleCause"
    // InternalDOF.g:439:1: entryRuleCause returns [EObject current=null] : iv_ruleCause= ruleCause EOF ;
    public final EObject entryRuleCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCause = null;


        try {
            // InternalDOF.g:439:46: (iv_ruleCause= ruleCause EOF )
            // InternalDOF.g:440:2: iv_ruleCause= ruleCause EOF
            {
             newCompositeNode(grammarAccess.getCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCause=ruleCause();

            state._fsp--;

             current =iv_ruleCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCause"


    // $ANTLR start "ruleCause"
    // InternalDOF.g:446:1: ruleCause returns [EObject current=null] : (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause ) ;
    public final EObject ruleCause() throws RecognitionException {
        EObject current = null;

        EObject this_CompoundCause_0 = null;

        EObject this_DataLinkedCause_1 = null;

        EObject this_NotMappedCause_2 = null;



        	enterRule();

        try {
            // InternalDOF.g:452:2: ( (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause ) )
            // InternalDOF.g:453:2: (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause )
            {
            // InternalDOF.g:453:2: (this_CompoundCause_0= ruleCompoundCause | this_DataLinkedCause_1= ruleDataLinkedCause | this_NotMappedCause_2= ruleNotMappedCause )
            int alt6=3;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==18) ) {
                int LA6_1 = input.LA(2);

                if ( (LA6_1==RULE_ID) ) {
                    switch ( input.LA(3) ) {
                    case 14:
                    case 25:
                        {
                        alt6=2;
                        }
                        break;
                    case 19:
                        {
                        int LA6_4 = input.LA(4);

                        if ( (LA6_4==RULE_STRING) ) {
                            switch ( input.LA(5) ) {
                            case 23:
                                {
                                alt6=3;
                                }
                                break;
                            case 14:
                                {
                                alt6=2;
                                }
                                break;
                            case 20:
                                {
                                alt6=1;
                                }
                                break;
                            default:
                                NoViableAltException nvae =
                                    new NoViableAltException("", 6, 7, input);

                                throw nvae;
                            }

                        }
                        else {
                            NoViableAltException nvae =
                                new NoViableAltException("", 6, 4, input);

                            throw nvae;
                        }
                        }
                        break;
                    case 23:
                        {
                        alt6=3;
                        }
                        break;
                    case 20:
                        {
                        alt6=1;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 2, input);

                        throw nvae;
                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalDOF.g:454:3: this_CompoundCause_0= ruleCompoundCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getCompoundCauseParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_CompoundCause_0=ruleCompoundCause();

                    state._fsp--;


                    			current = this_CompoundCause_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalDOF.g:463:3: this_DataLinkedCause_1= ruleDataLinkedCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getDataLinkedCauseParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_DataLinkedCause_1=ruleDataLinkedCause();

                    state._fsp--;


                    			current = this_DataLinkedCause_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalDOF.g:472:3: this_NotMappedCause_2= ruleNotMappedCause
                    {

                    			newCompositeNode(grammarAccess.getCauseAccess().getNotMappedCauseParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_NotMappedCause_2=ruleNotMappedCause();

                    state._fsp--;


                    			current = this_NotMappedCause_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCause"


    // $ANTLR start "entryRuleCompoundCause"
    // InternalDOF.g:484:1: entryRuleCompoundCause returns [EObject current=null] : iv_ruleCompoundCause= ruleCompoundCause EOF ;
    public final EObject entryRuleCompoundCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCompoundCause = null;


        try {
            // InternalDOF.g:484:54: (iv_ruleCompoundCause= ruleCompoundCause EOF )
            // InternalDOF.g:485:2: iv_ruleCompoundCause= ruleCompoundCause EOF
            {
             newCompositeNode(grammarAccess.getCompoundCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCompoundCause=ruleCompoundCause();

            state._fsp--;

             current =iv_ruleCompoundCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCompoundCause"


    // $ANTLR start "ruleCompoundCause"
    // InternalDOF.g:491:1: ruleCompoundCause returns [EObject current=null] : (otherlv_0= 'cause' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'contains' otherlv_5= '{' ( (lv_subCauses_6_0= ruleCause ) ) ( (lv_subCauses_7_0= ruleCause ) )* otherlv_8= '}' ) ;
    public final EObject ruleCompoundCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_realizes_3_0 = null;

        EObject lv_subCauses_6_0 = null;

        EObject lv_subCauses_7_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:497:2: ( (otherlv_0= 'cause' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'contains' otherlv_5= '{' ( (lv_subCauses_6_0= ruleCause ) ) ( (lv_subCauses_7_0= ruleCause ) )* otherlv_8= '}' ) )
            // InternalDOF.g:498:2: (otherlv_0= 'cause' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'contains' otherlv_5= '{' ( (lv_subCauses_6_0= ruleCause ) ) ( (lv_subCauses_7_0= ruleCause ) )* otherlv_8= '}' )
            {
            // InternalDOF.g:498:2: (otherlv_0= 'cause' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'contains' otherlv_5= '{' ( (lv_subCauses_6_0= ruleCause ) ) ( (lv_subCauses_7_0= ruleCause ) )* otherlv_8= '}' )
            // InternalDOF.g:499:3: otherlv_0= 'cause' ( (lv_name_1_0= RULE_ID ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'contains' otherlv_5= '{' ( (lv_subCauses_6_0= ruleCause ) ) ( (lv_subCauses_7_0= ruleCause ) )* otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getCompoundCauseAccess().getCauseKeyword_0());
            		
            // InternalDOF.g:503:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalDOF.g:504:4: (lv_name_1_0= RULE_ID )
            {
            // InternalDOF.g:504:4: (lv_name_1_0= RULE_ID )
            // InternalDOF.g:505:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(lv_name_1_0, grammarAccess.getCompoundCauseAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getCompoundCauseRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:521:3: (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==19) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalDOF.g:522:4: otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,19,FOLLOW_12); 

                    				newLeafNode(otherlv_2, grammarAccess.getCompoundCauseAccess().getRealizesKeyword_2_0());
                    			
                    // InternalDOF.g:526:4: ( (lv_realizes_3_0= ruleEString ) )
                    // InternalDOF.g:527:5: (lv_realizes_3_0= ruleEString )
                    {
                    // InternalDOF.g:527:5: (lv_realizes_3_0= ruleEString )
                    // InternalDOF.g:528:6: lv_realizes_3_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getCompoundCauseAccess().getRealizesEStringParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_13);
                    lv_realizes_3_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
                    						}
                    						set(
                    							current,
                    							"realizes",
                    							lv_realizes_3_0,
                    							"dataOrientedFishbone.DOF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,20,FOLLOW_14); 

            			newLeafNode(otherlv_4, grammarAccess.getCompoundCauseAccess().getContainsKeyword_3());
            		
            otherlv_5=(Token)match(input,21,FOLLOW_9); 

            			newLeafNode(otherlv_5, grammarAccess.getCompoundCauseAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalDOF.g:554:3: ( (lv_subCauses_6_0= ruleCause ) )
            // InternalDOF.g:555:4: (lv_subCauses_6_0= ruleCause )
            {
            // InternalDOF.g:555:4: (lv_subCauses_6_0= ruleCause )
            // InternalDOF.g:556:5: lv_subCauses_6_0= ruleCause
            {

            					newCompositeNode(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_15);
            lv_subCauses_6_0=ruleCause();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
            					}
            					add(
            						current,
            						"subCauses",
            						lv_subCauses_6_0,
            						"dataOrientedFishbone.DOF.Cause");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:573:3: ( (lv_subCauses_7_0= ruleCause ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==18) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalDOF.g:574:4: (lv_subCauses_7_0= ruleCause )
            	    {
            	    // InternalDOF.g:574:4: (lv_subCauses_7_0= ruleCause )
            	    // InternalDOF.g:575:5: lv_subCauses_7_0= ruleCause
            	    {

            	    					newCompositeNode(grammarAccess.getCompoundCauseAccess().getSubCausesCauseParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_15);
            	    lv_subCauses_7_0=ruleCause();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getCompoundCauseRule());
            	    					}
            	    					add(
            	    						current,
            	    						"subCauses",
            	    						lv_subCauses_7_0,
            	    						"dataOrientedFishbone.DOF.Cause");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_8=(Token)match(input,22,FOLLOW_2); 

            			newLeafNode(otherlv_8, grammarAccess.getCompoundCauseAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCompoundCause"


    // $ANTLR start "entryRuleDataLinkedCause"
    // InternalDOF.g:600:1: entryRuleDataLinkedCause returns [EObject current=null] : iv_ruleDataLinkedCause= ruleDataLinkedCause EOF ;
    public final EObject entryRuleDataLinkedCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDataLinkedCause = null;


        try {
            // InternalDOF.g:600:56: (iv_ruleDataLinkedCause= ruleDataLinkedCause EOF )
            // InternalDOF.g:601:2: iv_ruleDataLinkedCause= ruleDataLinkedCause EOF
            {
             newCompositeNode(grammarAccess.getDataLinkedCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDataLinkedCause=ruleDataLinkedCause();

            state._fsp--;

             current =iv_ruleDataLinkedCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDataLinkedCause"


    // $ANTLR start "ruleDataLinkedCause"
    // InternalDOF.g:607:1: ruleDataLinkedCause returns [EObject current=null] : (otherlv_0= 'cause' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'is' ( (lv_dataFeeder_5_0= ruleDataFeeder ) ) ) ;
    public final EObject ruleDataLinkedCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        AntlrDatatypeRuleToken lv_realizes_3_0 = null;

        EObject lv_dataFeeder_5_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:613:2: ( (otherlv_0= 'cause' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'is' ( (lv_dataFeeder_5_0= ruleDataFeeder ) ) ) )
            // InternalDOF.g:614:2: (otherlv_0= 'cause' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'is' ( (lv_dataFeeder_5_0= ruleDataFeeder ) ) )
            {
            // InternalDOF.g:614:2: (otherlv_0= 'cause' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'is' ( (lv_dataFeeder_5_0= ruleDataFeeder ) ) )
            // InternalDOF.g:615:3: otherlv_0= 'cause' ( (lv_name_1_0= ruleQualifiedName ) ) (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )? otherlv_4= 'is' ( (lv_dataFeeder_5_0= ruleDataFeeder ) )
            {
            otherlv_0=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getDataLinkedCauseAccess().getCauseKeyword_0());
            		
            // InternalDOF.g:619:3: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalDOF.g:620:4: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalDOF.g:620:4: (lv_name_1_0= ruleQualifiedName )
            // InternalDOF.g:621:5: lv_name_1_0= ruleQualifiedName
            {

            					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getNameQualifiedNameParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_16);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"dataOrientedFishbone.DOF.QualifiedName");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalDOF.g:638:3: (otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) ) )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // InternalDOF.g:639:4: otherlv_2= 'realizes' ( (lv_realizes_3_0= ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,19,FOLLOW_12); 

                    				newLeafNode(otherlv_2, grammarAccess.getDataLinkedCauseAccess().getRealizesKeyword_2_0());
                    			
                    // InternalDOF.g:643:4: ( (lv_realizes_3_0= ruleEString ) )
                    // InternalDOF.g:644:5: (lv_realizes_3_0= ruleEString )
                    {
                    // InternalDOF.g:644:5: (lv_realizes_3_0= ruleEString )
                    // InternalDOF.g:645:6: lv_realizes_3_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getRealizesEStringParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_5);
                    lv_realizes_3_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
                    						}
                    						set(
                    							current,
                    							"realizes",
                    							lv_realizes_3_0,
                    							"dataOrientedFishbone.DOF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,14,FOLLOW_3); 

            			newLeafNode(otherlv_4, grammarAccess.getDataLinkedCauseAccess().getIsKeyword_3());
            		
            // InternalDOF.g:667:3: ( (lv_dataFeeder_5_0= ruleDataFeeder ) )
            // InternalDOF.g:668:4: (lv_dataFeeder_5_0= ruleDataFeeder )
            {
            // InternalDOF.g:668:4: (lv_dataFeeder_5_0= ruleDataFeeder )
            // InternalDOF.g:669:5: lv_dataFeeder_5_0= ruleDataFeeder
            {

            					newCompositeNode(grammarAccess.getDataLinkedCauseAccess().getDataFeederDataFeederParserRuleCall_4_0());
            				
            pushFollow(FOLLOW_2);
            lv_dataFeeder_5_0=ruleDataFeeder();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDataLinkedCauseRule());
            					}
            					set(
            						current,
            						"dataFeeder",
            						lv_dataFeeder_5_0,
            						"dataOrientedFishbone.DOF.DataFeeder");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDataLinkedCause"


    // $ANTLR start "entryRuleNotMappedCause"
    // InternalDOF.g:690:1: entryRuleNotMappedCause returns [EObject current=null] : iv_ruleNotMappedCause= ruleNotMappedCause EOF ;
    public final EObject entryRuleNotMappedCause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNotMappedCause = null;


        try {
            // InternalDOF.g:690:55: (iv_ruleNotMappedCause= ruleNotMappedCause EOF )
            // InternalDOF.g:691:2: iv_ruleNotMappedCause= ruleNotMappedCause EOF
            {
             newCompositeNode(grammarAccess.getNotMappedCauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleNotMappedCause=ruleNotMappedCause();

            state._fsp--;

             current =iv_ruleNotMappedCause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNotMappedCause"


    // $ANTLR start "ruleNotMappedCause"
    // InternalDOF.g:697:1: ruleNotMappedCause returns [EObject current=null] : ( () otherlv_1= 'cause' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'realizes' ( (lv_realizes_4_0= ruleEString ) ) )? otherlv_5= 'notMapped' ) ;
    public final EObject ruleNotMappedCause() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_name_2_0=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        AntlrDatatypeRuleToken lv_realizes_4_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:703:2: ( ( () otherlv_1= 'cause' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'realizes' ( (lv_realizes_4_0= ruleEString ) ) )? otherlv_5= 'notMapped' ) )
            // InternalDOF.g:704:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'realizes' ( (lv_realizes_4_0= ruleEString ) ) )? otherlv_5= 'notMapped' )
            {
            // InternalDOF.g:704:2: ( () otherlv_1= 'cause' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'realizes' ( (lv_realizes_4_0= ruleEString ) ) )? otherlv_5= 'notMapped' )
            // InternalDOF.g:705:3: () otherlv_1= 'cause' ( (lv_name_2_0= RULE_ID ) ) (otherlv_3= 'realizes' ( (lv_realizes_4_0= ruleEString ) ) )? otherlv_5= 'notMapped'
            {
            // InternalDOF.g:705:3: ()
            // InternalDOF.g:706:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getNotMappedCauseAccess().getNotMappedCauseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,18,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getNotMappedCauseAccess().getCauseKeyword_1());
            		
            // InternalDOF.g:716:3: ( (lv_name_2_0= RULE_ID ) )
            // InternalDOF.g:717:4: (lv_name_2_0= RULE_ID )
            {
            // InternalDOF.g:717:4: (lv_name_2_0= RULE_ID )
            // InternalDOF.g:718:5: lv_name_2_0= RULE_ID
            {
            lv_name_2_0=(Token)match(input,RULE_ID,FOLLOW_17); 

            					newLeafNode(lv_name_2_0, grammarAccess.getNotMappedCauseAccess().getNameIDTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getNotMappedCauseRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_2_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalDOF.g:734:3: (otherlv_3= 'realizes' ( (lv_realizes_4_0= ruleEString ) ) )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==19) ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalDOF.g:735:4: otherlv_3= 'realizes' ( (lv_realizes_4_0= ruleEString ) )
                    {
                    otherlv_3=(Token)match(input,19,FOLLOW_12); 

                    				newLeafNode(otherlv_3, grammarAccess.getNotMappedCauseAccess().getRealizesKeyword_3_0());
                    			
                    // InternalDOF.g:739:4: ( (lv_realizes_4_0= ruleEString ) )
                    // InternalDOF.g:740:5: (lv_realizes_4_0= ruleEString )
                    {
                    // InternalDOF.g:740:5: (lv_realizes_4_0= ruleEString )
                    // InternalDOF.g:741:6: lv_realizes_4_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getNotMappedCauseAccess().getRealizesEStringParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_18);
                    lv_realizes_4_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getNotMappedCauseRule());
                    						}
                    						set(
                    							current,
                    							"realizes",
                    							lv_realizes_4_0,
                    							"dataOrientedFishbone.DOF.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }

            otherlv_5=(Token)match(input,23,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getNotMappedCauseAccess().getNotMappedKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNotMappedCause"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalDOF.g:767:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalDOF.g:767:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalDOF.g:768:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;

             current =iv_ruleQualifiedNameWithWildcard.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalDOF.g:774:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalDOF.g:780:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalDOF.g:781:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalDOF.g:781:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalDOF.g:782:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {

            			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
            		
            pushFollow(FOLLOW_19);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;


            			current.merge(this_QualifiedName_0);
            		

            			afterParserOrEnumRuleCall();
            		
            // InternalDOF.g:792:3: (kw= '.*' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==24) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalDOF.g:793:4: kw= '.*'
                    {
                    kw=(Token)match(input,24,FOLLOW_2); 

                    				current.merge(kw);
                    				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalDOF.g:803:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalDOF.g:803:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalDOF.g:804:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
             newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;

             current =iv_ruleQualifiedName.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalDOF.g:810:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        Token kw=null;
        Token this_ID_2=null;


        	enterRule();

        try {
            // InternalDOF.g:816:2: ( (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* ) )
            // InternalDOF.g:817:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            {
            // InternalDOF.g:817:2: (this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )* )
            // InternalDOF.g:818:3: this_ID_0= RULE_ID (kw= '.' this_ID_2= RULE_ID )*
            {
            this_ID_0=(Token)match(input,RULE_ID,FOLLOW_20); 

            			current.merge(this_ID_0);
            		

            			newLeafNode(this_ID_0, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_0());
            		
            // InternalDOF.g:825:3: (kw= '.' this_ID_2= RULE_ID )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==25) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalDOF.g:826:4: kw= '.' this_ID_2= RULE_ID
            	    {
            	    kw=(Token)match(input,25,FOLLOW_3); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	    			
            	    this_ID_2=(Token)match(input,RULE_ID,FOLLOW_20); 

            	    				current.merge(this_ID_2);
            	    			

            	    				newLeafNode(this_ID_2, grammarAccess.getQualifiedNameAccess().getIDTerminalRuleCall_1_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleEString"
    // InternalDOF.g:843:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalDOF.g:843:47: (iv_ruleEString= ruleEString EOF )
            // InternalDOF.g:844:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalDOF.g:850:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : this_STRING_0= RULE_STRING ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;


        	enterRule();

        try {
            // InternalDOF.g:856:2: (this_STRING_0= RULE_STRING )
            // InternalDOF.g:857:2: this_STRING_0= RULE_STRING
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            		current.merge(this_STRING_0);
            	

            		newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000060002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000460000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000084000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000880000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000002000002L});

}