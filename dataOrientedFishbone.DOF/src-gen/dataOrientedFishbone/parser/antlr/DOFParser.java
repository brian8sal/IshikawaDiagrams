/*
 * generated by Xtext 2.24.0
 */
package dataOrientedFishbone.parser.antlr;

import com.google.inject.Inject;
import dataOrientedFishbone.parser.antlr.internal.InternalDOFParser;
import dataOrientedFishbone.services.DOFGrammarAccess;
import org.eclipse.xtext.parser.antlr.AbstractAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;

public class DOFParser extends AbstractAntlrParser {

	@Inject
	private DOFGrammarAccess grammarAccess;

	@Override
	protected void setInitialHiddenTokens(XtextTokenStream tokenStream) {
		tokenStream.setInitialHiddenTokens("RULE_WS", "RULE_ML_COMMENT", "RULE_SL_COMMENT");
	}
	

	@Override
	protected InternalDOFParser createParser(XtextTokenStream stream) {
		return new InternalDOFParser(stream, getGrammarAccess());
	}

	@Override 
	protected String getDefaultRuleName() {
		return "DOF";
	}

	public DOFGrammarAccess getGrammarAccess() {
		return this.grammarAccess;
	}

	public void setGrammarAccess(DOFGrammarAccess grammarAccess) {
		this.grammarAccess = grammarAccess;
	}
}
