/*
 * generated by Xtext 2.24.0
 */
package dataOrientedFishbone.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class DOFAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("dataOrientedFishbone/parser/antlr/internal/InternalDOF.tokens");
	}
}
